import argparse
import textwrap

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# import seaborn as sns

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Make a figurage comparing the coverage', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--cov_reveal',
                        required=True,
                        nargs='*',
                        help=textwrap.dedent('''\
                        list of coverage file of reveal
                        ''')
                        )
    parser.add_argument('--cov_bestfit',
                        required=True,
                        nargs='*',
                        help=textwrap.dedent('''\
                        list of coverage file of bestfit
                        ''')
                        )
    parser.add_argument('--cov_caspots',
                        required=True,
                        nargs='*',
                        help=textwrap.dedent('''\
                        list of coverage file of caspots
                        ''')
                        )

    parser.add_argument('--cov_our',
                        required=True,
                        nargs='*',
                        help=textwrap.dedent('''\
                        list of coverage file of our method
                        ''')
                        )

    parser.add_argument('--measure',
                        type=str,
                        required=True,
                        choices=[
                            "retreived_edges__number",
                            "retreived_edges__proportion",
                            "retreived_paths__number",
                            "retreived_paths__proportion",
                            "retreived_edges_all__number",
                            "retreived_edges_all__proportion",
                            "retreived_paths_all__number",
                            "retreived_paths_all__proportion"
                        ],
                        help=textwrap.dedent('''\
                        Measure with which constructing the boxplot.
                        ''')
                        )

    parser.add_argument('--output',
                        required=True,
                        help=textwrap.dedent('''\
                        where to save the figure
                        ''')
                        )

    updatescheme = "mixed"

    args = parser.parse_args()

    print(args)
    print(".......................")
    hline = set()

    datapoint_reveal = []
    for coveragefile in args.cov_reveal:
        print(coveragefile)
        covtable = pd.read_csv(coveragefile, index_col=0)
        print(covtable)
        datapoint_reveal.append(
            covtable[updatescheme][args.measure])
        hline.add(covtable["obs"][args.measure])
    #assert(len(datapoint_bestfit) == 1)

    datapoint_bestfit = []
    for coveragefile in args.cov_bestfit:
        print(coveragefile)
        covtable = pd.read_csv(coveragefile, index_col=0)
        print(covtable)
        datapoint_bestfit.append(
            covtable[updatescheme][args.measure])
        hline.add(covtable["obs"][args.measure])
    #assert(len(datapoint_bestfit) == 1)

    datapoints_caspots = []
    for coveragefile in args.cov_caspots:
        print(coveragefile)
        covtable = pd.read_csv(coveragefile, index_col=0)
        print(covtable)
        datapoints_caspots.append(covtable[updatescheme]
                                  [args.measure])
        hline.add(covtable["obs"][args.measure])

    datapoint_our = []
    for coveragefile in args.cov_our:
        print(coveragefile)
        covtable = pd.read_csv(coveragefile, index_col=0)
        print(covtable)
        datapoint_our.append(
            covtable[updatescheme][args.measure])
        hline.add(covtable["obs"][args.measure])

    #assert(len(datapoint_our) == 1)

    if all(np.isnan(list(hline))):
        hline = {0}
    assert(len(hline) == 1)

    data = [datapoint_reveal, datapoint_bestfit,
            datapoints_caspots, datapoint_our]

    plt.figure(figsize=(3, 3))
    plt.axhline(hline.pop(), c='blue', linestyle="--",
                linewidth=0.5, alpha=0.9)
    plt.boxplot(data,
                # Positions defaults to range(1, N+1) where N is the number of boxplot to be drawn.
                # lets reduce the space between them
                positions=[1, 1.5, 2, 2.5],
                labels=[
                    f"REVEAL\n({len(datapoint_reveal)} BNs)",
                    f"Best-Fit\n({len(datapoint_bestfit)} BNs)",
                    f"caspo-TS\n({len(datapoints_caspots)} BNs)",
                    f"ASKeD-BN\n({len(datapoint_our)} BNs)"
                ])
    plt.xticks(rotation=45)
    plt.ylabel('# retrieved transitions')
    plt.tight_layout()
    plt.savefig(args.output)

    #
    # sns_boxplots = sns.boxplot(
    #     data=[datapoints_caspots, datapoint_bestfit, datapoint_our],
    #     # palette=[sns.xkcd_rgb["pale red"], sns.xkcd_rgb["medium green"]],
    #     # showmeans=True,
    # )
    # # figure = sns_boxplots.get_figure()
    # # figure.savefig(args.output)
    # plt.savefig(args.output)

    # Positions defaults to range(1, N+1) where N is the number of boxplot to be drawn.
    # we will move them a little, to visually group them
    # plt.figure(figsize=(10, 6))
    # box = plt.boxplot(data_to_plot,
    # positions=[1, 1.6, 2.5, 3.1, 4, 4.6, 5.5, 6.1],
    # labels=['A1', 'A0', 'B1', 'B0', 'C1', 'C0', 'D1', 'D0'])

    if len(datapoints_caspots) > 1:
        print("how many BNs have the max coverage in caspots :")
        print(datapoints_caspots.count(max(datapoints_caspots)))
