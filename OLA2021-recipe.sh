
snakemake -s OLA2021.smk all_aggregation_results_image --cores 1 -p --configfile xpconfig_OLA21_rerunall_202106.yml --forcerun aggregation_results_image
snakemake -s OLA2021.smk all_figure_comparisoncoverage --cores 1 -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp --forcerun figure_comparisoncoverage -n





snakemake -s OLA2021.smk all_numberhyperedgesJOBIM2021 --cores 1  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml   -n



# === Do ALL automatically ===
snakemake -s OLA2021.smk all --cores 30  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp --keep-going

# === Get data ===

# --- All in one go :
snakemake -s OLA2021.smk all_setupdata --cores 30  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp --keep-going

# --- Get example data (Akutsu) :
snakemake -s OLA2021.smk generate_Akutsuexample --cores 1  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp
# --- Get the real data :
snakemake -s OLA2021.smk generate_realdatasets --cores 1  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp
# --- Get all generated PKN and TS for PyBoolNet systems
snakemake -s OLA2021.smk all_data4PyBoolNetsystems --cores 4  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp --keep-going



# === BNs synthesis ===

# --- Example to get lp file for a specific system :
# -> for faure_cellcycle_synchronous_noise0.1_seed13 (Note that CycD should be always binarized at 1)
# snakemake -s BN-identification.smk experiments/xpname/faure_cellcycle_synchronous_noise0.1_seed13/generate_lp.done  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp --keep-going


# --- Run all the methods on all the systems :
snakemake -s OLA2021.smk all_BNsidentification --cores 40  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp --keep-going


# === BNs analysis ===

# --- Get all coverage files for the given update scheme.
# !!! cette étape pose soucis !!!

# without batch :
snakemake -s OLA2021.smk all_get_coverage_stgVSts --cores 5  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp -k
# with batch :
# better in cases there are a lot of bnet to process.
# the batch can only be performed on aggregations rules, which in this case is all_get_coverage_stgVSts
# https://github.com/snakemake/snakemake/issues/98
for i in {1..10}
do
  snakemake -s OLA2021.smk all_get_coverage_stgVSts --cores 5  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp -k --batch all_get_coverage_stgVSts=$i/10
done

# --- Get comparative coverage boxplot
# comparative boxplots for OLA21 submission.
# one figure per system, one boxplot per identification method (for a given coverage measure and stg update scheme)

snakemake -s OLA2021.smk all_figure_comparisoncoverage --cores 1  -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp --forcerun figure_comparisoncoverage
snakemake -s OLA2021.smk all_figure_comparisoncoverage_PyBoolNetrepo --cores 1  -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp --forcerun figure_comparisoncoverage


# --- Get table number of BNs generated for all systems by all the identification methods
snakemake -s OLA2021.smk all_number_generatedBNs --cores 1  -p --configfile xpconfig_OLA21_rerunall_202106.yml

snakemake -s OLA2021.smk all_aggregationresults --cores 1  -p --configfile xpconfig_OLA21_rerunall_202106.yml


snakemake -s OLA2021.smk all_aggregation_results_image --cores 1  -p --configfile xpconfig_OLA21_rerunall_202106.yml





# === Other examples ===


# --- Get all coverage boxplot
# They are individual boxplot drawn for a specific identification method, coverage measure, and update scheme
snakemake -s OLA2021.smk all_coverageboxplot --cores 5  --reason -p --con


# --- Get all stgsif
# snakemake -s BN-analysis.smk all_bnet2stgsif --cores 5  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp -k
# --- Get all stgpng
# batch version :
for i in {1..10}
do
  snakemake -s plots.smk all_stgpng --cores 7  --reason -p --configfile xpconfig_RBoolNet-yeast.yml  --notemp --batch all_stgpng=$i/10
done
# no batch :
snakemake -s plots.smk all_stgpng --cores 7  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp













#========================================================
# Anciennes recette (non retestées)
# --- Get subset stgpng async
# snakemake -s OLA2021.smk subset_stgpng_async --cores 4  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp

# --- Get boxplots :
# snakemake -s BN-analysis.smk all_coverageboxplot  --cores 7  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml  --notemp --forcerun get_coverageboxplot
# ---  Get the report:
# python3 code/report_results-analysis.py --xprespath experiments/xpname
# --- Get image about the example for the paper :
# snakemake -s OLA2021.smk figpaper_all  --cores 7  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml

# --- Get ts plots:
# snakemake -s plots.smk all_tsplots  --cores 5  --reason -p --configfile xpconfig_OLA21_rerunall_202106.yml -k
