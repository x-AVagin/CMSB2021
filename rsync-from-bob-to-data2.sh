rsync -azPv --exclude-from .rsyncignore . avaginay@sb-data:/data2/avaginay/BM2BN/

# -a : archive mode :
#   recursive
#   preserve symbolic links (but not hard ones)
# 	preserve permission
#   preserve dates
#   preserve group
#   preserve ownership
#   preserve peripherals
# -z : compression
# -P : show progress
# -v : verbose

# Other usefull option :
# -u : update = ignore files that are more recent on the cluster
