
filename=environement_$CONDA_DEFAULT_ENV.yml
conda env export --no-builds > $filename


current_timestamp=`date +"%Y%m%d-%Hh%M"`
sed -i "s/^name:.*$/name: ${CONDA_DEFAULT_ENV}_${current_timestamp}/g" $filename


# use a tempfile because grep cannot process an input file that is also the output
grep -v "^prefix: " $filename > temp
mv temp $filename


echo "Current conda env has been exported in $filename file" 
