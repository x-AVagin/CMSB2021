
python3 code/runsimu.py  data/raw_BIOMD0000000111/BIOMD0000000111_urn.xml

python3 code/runsimu.py  data/raw_BIOMD0000000065/BIOMD000000065_url.xml



for filename in /home/nanis/Documents/TRAVAIL/THESE/PROJETS/202006_Biomodels2BN/dataset/BioModels_Database-r31_pub-sbml_files/curated/*xml; do
  echo $filename
python3 code/runsimu.py  $filename
done
# Takes an sbml
# Outputs a timecourse file as res-simu.txt
# *in the same directory than the input file*


# Takes an sbml file + res-simu.csv file + an output directory path
# Outputs the pkn and observation as ASP for each agent
python3 sbml2lp.py data/raw_BIOMD0000000111/BIOMD0000000111_urn.xml data/raw_BIOMD0000000111/res-simu.txt data/lp_BIOMD0000000111/


# for each component [TODO]
# Lauch glingo on the lp files :
clingo -n 0 data/lp_BIOMD0000000111/knowledge_cdc13t.lp code/identify.lp
