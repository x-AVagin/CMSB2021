configfile: "xpconfig.yml"

include: "BM2BN.smk"
include: "sbml2data.smk"
include: "data-info.smk"
include: "BN-identification.smk"
include: "BN-analysis.smk"
include: "plots.smk"


onstart:
    try:
        sys.path.insert(1, 'code/')
        import logger
        logger.set_loggers()
    except ImportError:
        print("is it the correct working directory ?")
        # %pwd
    import logging

    logger = logging.getLogger()
    logger.debug(f"sm pipeline starts.")
onsuccess:
    logger.info(f"sm pipeline succeeded.")
onerror:
    logger.error(f"sm pipeline failed.")
