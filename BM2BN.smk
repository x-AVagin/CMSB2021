import glob
import itertools
import multiprocessing
import os
import pathlib
import re
import sys

import pandas as pd
import yaml

try:
    sys.path.insert(1, 'code/')
    import sbmlparser
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


wildcard_constraints:
    # (= it starts by BIOMD followed by 10 numbers)
    BIOMD_id = "BIOMD[0-9]{10}",
    # BN_id = "[0-9]+"


for key in (
        "folder_sbml",
        "folder_taskdescr",
        "folder_curationReport",
        "folder_pkn",
        "folder_ts",
        "folder_lp",
        "folder_bn",
        "folder_heatmapsCompflictingTimeSteps",
        "folder_xp",
        "file_sbmlList"
):
    print("->" + key)
    print(config[key])
    config[key] = pathlib.Path(config[key])


def get_nodesTaken(BIOMD_ids, safe=False):
    """
    Argument:
    ---------
    BIOMD_ids : list of BIOMD_ids
    """
    nodesTaken_perBIOMDid = []
    print(config["nodesTypes"])
    for BIOMD_id in BIOMD_ids:
        print(BIOMD_id)
        sbmlfilepath = config["folder_sbml"] / f"{BIOMD_id}.xml"
        print(sbmlfilepath)
        model = sbmlparser.sbmlfile2libsbmlmodel(sbmlfilepath)
        nodesTaken = sbmlparser.getListOfNodes(
            model,
            config["nodesTypes"],
            safe=safe)
        nodesTaken_perBIOMDid.append(nodesTaken)
    print(nodesTaken_perBIOMDid)
    return nodesTaken_perBIOMDid


gW_BIOMD_id, = glob_wildcards(config["folder_sbml"] / "{BIOMD_id}.xml")
print(gW_BIOMD_id)


# Custom expand function


def get_BIOMDid2nodes_mapping(BIOMD_ids, nodeIDs_perfile):
    """
    Arguments :
    -----------
    - BIOMD_ids: list of BIOMD_ids
    - nodeIDs_perfile: list of list of nodes for each BIOMD_id in BIOMD_ids

    Returns:
    --------
    list of tuples (BIOMD_id, node_id) for each node_id in nodeIDs corresponding to the associated BIOMD_id sbml

    Example:
    --------
    >>> BIOMD_ids = ["file1", "file2"]
    >>> nodeIDs_perfile = [["node1A", "node1B"], ["node2A", "node2B"]]
    >>> get_BIOMDid2nodes_mapping(BIOMD_ids, nodeIDs_perfile)
    [('file1', 'node1A'), ('file1', 'node1B'),
      ('file2', 'node2A'), ('file2', 'node2B')]
    """
    assert(len(BIOMD_ids) == len(nodeIDs_perfile))
    return [(BIOMD_id, node) for BIOMD_id, nodesIDs in zip(BIOMD_ids, nodeIDs_perfile) for node in nodesIDs]


def aggregate_bnetfiles_(wildcards):
    # input function for the rule aggregate

    bnet_folders_fromcheckpoints = []

    if "our" in config["identification_methods"]:
        bnet_folders_fromcheckpoints.extend(checkpoints.lp2bn.get(
            SYSTEM_id=BIOMD_id).output for BIOMD_id in gW_BIOMD_id)

    if "reveal" in config["identification_methods"]:
        bnet_folders_fromcheckpoints.extend(checkpoints.bnet_reveal.get(
            SYSTEM_id=BIOMD_id).output for BIOMD_id in gW_BIOMD_id)

    if "bestfit" in config["identification_methods"]:
        bnet_folders_fromcheckpoints.extend(checkpoints.bnet_bestfit.get(
            SYSTEM_id=BIOMD_id).output for BIOMD_id in gW_BIOMD_id)

    # bnet_folders_fromcheckpoints is a list of lists containing one element : the folder that was outputed by the checkpoint
    bnet_files = [list(pathlib.Path(bn_folder).glob("*.bnet"))
                  for bn_folder in itertools.chain.from_iterable(bnet_folders_fromcheckpoints)]
    bnet_files_flatList = list(itertools.chain.from_iterable(bnet_files))

    bnet_files_flatList = [str(path) for path in bnet_files_flatList]
    print(bnet_files_flatList)
    return bnet_files_flatList


# By default snakemake executes the first rule in the snakefile.
# This gives rise to pseudo-rules at the beginning of the file that can be used to define build-targets similar to GNU Make
# Snakemake also accepts rule names as targets if the referred rule does not have wildcards -> the rule `all` can not have wildcards
rule all_BM2BN:
    input:
        # list of sbml files taken into account :
        config["file_sbmlList"],
        # list of sbml files throwing warnings when opening them :
        config["file_sbmlList"].parents[0] / \
            "sbml-with-opening-warnings_List",
        # taskdescr files :
        expand(
            config["folder_taskdescr"] / "{BIOMD_id}.yaml", BIOMD_id=gW_BIOMD_id
        ),
        # simulations :
        expand(
            config['folder_ts'] / "{BIOMD_id}_ts.csv", BIOMD_id=gW_BIOMD_id
        ),
        # pkn :
        expand(
            config['folder_pkn'] / "{BIOMD_id}.sif", BIOMD_id=gW_BIOMD_id
        ),
        # One lp file per node taken into account in the sbml
        # Either do (does not work because following sm error "Wildcards in input files cannot be determined from output file"):
        # [config["folder_lp"] / "{BIOMD_id}" / "knowledge_{node}.lp" for BIOMD_id, node in get_BIOMDid2nodes_mapping(gW_BIOMD_id, get_nodesTaken(gW_BIOMD_id))],
        # Or with the expand with a custom function with map the BIOMD_id to its nodes (does not work):
        # expand(
        #    config['folder_lp'] / "{BIOMD_id}" /"knowledge_ts-{node}.lp",
        #    get_BIOMDid2nodes_mapping,
        #    BIOMD_id=gW_BIOMD_id,
        #    node = get_nodesTaken(gW_BIOMD_id)
        # ),
        # Or just call other lp files and lp files for nodes will be generating along.
        # Problem, since they are defined dynamically, they would be regenerated everytime the pipeline is ran.
        # other lp files :
        # expand(config['folder_lp'] / "{BIOMD_id}" /
        #    "knowledge_ts-stretched.lp", BIOMD_id=gW_BIOMD_id) if config["optimization"] == "mae" else "",
        # expand(config['folder_lp'] / "{BIOMD_id}" /
        #    "knowledge_ts-binarized.lp", BIOMD_id=gW_BIOMD_id) if config["optimization"] == "mae" else "",
        # Just call the task.done file, and all the other lp files will be generated along :
        expand(config["folder_lp"] / "{BIOMD_id}" /
               "generate_lp.done", BIOMD_id=gW_BIOMD_id),
        # bnet results : At least 1 bn will be generated : 0.bnet
        # folder are not taken into account in input rule.
        # but we can gather outputs of checkpont rule
        # with a re-evaluation of the chekpoint and access to its output with this function
        aggregate_bnetfiles_,
        # dynamic(
        #    config["folder_bn"] / "{BIOMD_id}" / "{BN_id}.bnet"),
        # expand(
        #    config["folder_bn"] / "{BIOMD_id}" / "{BN_id}_stg-synchronous_focusedbts.sif", BN_id=gW.BN_id),
        # expand(
        #    config["folder_bn"] / "{BIOMD_id}" / "{BN_id}_stg-synchronous_focusedbts.png", BN_id=gW.BN_id),
        # dynamic(
        #    config["folder_bn"] / "{BIOMD_id}" / "{BN_id}_stg-synchronous.png")


gW_BIOMDid_generatedbnet, gW_identification_method, gW_BNid_generatedbnet = glob_wildcards(
    config["folder_bn"] / "{BIOMD_id}" / "{identification_method}" / "{BN_id}.bnet")
print("generated bnet:")
print(gW_BIOMDid_generatedbnet)
print(gW_identification_method)
print(gW_BNid_generatedbnet)


rule sbmllist:
    input:
        # all the sbml file in the given glob
        sbmlfiles = list(config["folder_sbml"].glob("*.xml"))
    output:
        config["file_sbmlList"]
    shell:
        "ls {config[folder_sbml]}/*.xml -1 > {output}"

rule sbmlhavingwarnings:
    input:
        # all the sbml file in the given glob
        sbmlfiles = list(config["folder_sbml"].glob("*.xml"))
    output:
        config["file_sbmlList"].parents[0] / \
            "sbml-with-opening-warnings_List"
    run:
        with open(config["file_sbmlList"].parents[0] /
                  "sbml-with-opening-warnings_List", "w") as fout:
            for sbmlfilepath in input.sbmlfiles:
                if sbmlparser.haserrorwhileopening(sbmlfilepath):
                    fout.write(f"{sbmlfilepath}\n")


rule sbmllist_withcuratedts:
    input:
        odscurationtimes = "table_curatedBIOMD_time-curation-ts.ods",
        sbmllist_xp = config["file_sbmlList"]
    output:
        config["file_sbmlList_withcratedts"]
    run:
        # get the sbml part of the xp
        with open(input.sbmllist_xp, 'r') as fin:
            sbmlofxp_paths = fin.readlines()
        sbmlofxp_filenames = [os.path.basename(p).strip()
                              for p in sbmlofxp_paths]
        # get list of sbml from biomodels having a curated ts
        curation_df = pd.read_excel(
            "table_curatedBIOMD_time-curation-ts.ods",
            index_col=0,  # BIOMDID
            engine="odf")
        curation_df.head()
        len(curation_df.index)
        sbmllist_withcuratedts = curation_df.dropna(subset=["stop"])
        # take the filename
        # also : rm duplicates (might have some if several curation images about ts had different info)
        sbmllist_withcuratedts = set(sbmllist_withcuratedts.filename)
        len(sbmllist_withcuratedts)
        # get the sbml of the with that have a curated ts :
        sbmlofxp_withcuratedts = set(
            sbmlofxp_filenames).intersection(sbmllist_withcuratedts)
        with open(config["file_sbmlList_withcratedts"], "w") as fout:
            for e in sbmlofxp_withcuratedts:
                fout.write(f"{e}\n")


rule get_curationReport:
    input:
        config["file_sbmlList"]
    params:
        config["folder_curationReport"]
    output:
        config["folder_curationReport"] / \
            "report_curation-from-BIOMODELS.html"
    shell:
        "python3 code/htmlreport_length-curated-simulation_Biomodels.py --sbmlList {input} --outfolderpath {params}"


rule compare_ts_VS_curatedts:
    input:
        # all the plots of raw ts
        expand(
            config['folder_ts'] / "{BIOMD_id}_tsraw.png", BIOMD_id=gW_BIOMD_id
        ),
        # all the bts
        expand(
            config['folder_ts'] / "{BIOMD_id}_binarizedtsts.csv", BIOMD_id=gW_BIOMD_id
        )
        # the images of the curated ts from the Biomodels website
        # "SAVE_report_curation-allBIOMODELScurated/report_curation-from-BIOMODELS.html"
    params:
        folder_xp = config["folder_xp"]
    output:
        # directory containing htlm report
        directory(config['folder_ts'] / "htmlreport_compare-ts-with-curatedts")
    shell:
        "python3 code/htmlreport_compare-ts-with-curatedts.py --xprespath {params.folder_xp}"


# Extract influences from sbml file
# store them in a dot file
rule sbml2dot:
    input:
        sbml = config["folder_sbml"] / "{BIOMD_id}.xml"
    params:
        " ".join(config["nodesTypes"])
    output:
        config['folder_pkn'] / "{BIOMD_id}.dot"
    shell:
        "python3 code/sbml2dot.py --sbmlfilepath {input.sbml} --nodesTypes {params}  --dotfilepath {output}"


include: "sbml2data.smk"
include: "data-info.smk"
include: "BN-identification.smk"
include: "BN-analysis.smk"
include: "plots.smk"
