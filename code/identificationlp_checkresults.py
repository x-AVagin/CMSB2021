import itertools
import sys

import clyngor
import networkx as nx
import PyBoolNet

import matplotlib.pyplot as plt

try:
    sys.path.insert(1, 'code/')
    from simplify import clyngorAnswer2sympy
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


# -------
lpfile = "data/lp_BIOMD0000000111/knowledge_cdc13t.lp"
answers = clyngor.solve(
    [lpfile, 'code/identify.lp'],
    options=['--opt-mode=optN', '--quiet=1,0,0']
    # https://sourceforge.net/p/potassco/mailman/message/34311933/
)
answers = list(answers)
len(answers)

for answer_idx, answer in enumerate(answers):
    print("------", answer_idx)
    answerSympy = clyngorAnswer2sympy(answer)


# -------


for bnet_possibility in itertools.product(*(bnet_constructor[x] for x in agents)):
    print("----------")
    print(bnet_possibility)

    bnet = ''
    for agent, expression in zip(agents, bnet_possibility):
        expression = expression.replace("~", "!")

        bnet += f"{agent.upper()}, {expression.upper()} \n"

    print(bnet)

    primes = PyBoolNet.FileExchange.bnet2primes(bnet)
    primes
    updatescheme = "synchronous"

    stg = PyBoolNet.StateTransitionGraphs.primes2stg(primes, updatescheme)
    steady, cyclic = PyBoolNet.Attractors.compute_attractors_tarjan(stg)
    print(agents)
    print(steady)
    print(cyclic)
    len(cyclic)


bnet = """
"""

# %%

primes = PyBoolNet.FileExchange.bnet2primes(bnet)
primes
updatescheme = "synchronous"

stg = PyBoolNet.StateTransitionGraphs.primes2stg(primes, updatescheme)
steady, cyclic = PyBoolNet.Attractors.compute_attractors_tarjan(stg)
# A B L Le M P
print(steady)
print(cyclic)
len(cyclic)


# %%
primes = PyBoolNet.FileExchange.bnet2primes(bnet)
primes

const = PyBoolNet.PrimeImplicants.find_constants(primes)
print(const)


# %% interaction graph

PyBoolNet.InteractionGraphs.create_image(primes, "igraph.dot")
PyBoolNet.InteractionGraphs.create_image(
    primes, "igraph2.dot", Styles=["anonymous", "sccs"])


PyBoolNet.InteractionGraphs.create_image(
    primes, "igraph3.dot", Styles=["sccs"])


state = PyBoolNet.StateTransitionGraphs.random_state(primes)
local_igraph = PyBoolNet.InteractionGraphs.local_igraph_of_state(primes, state)
PyBoolNet.InteractionGraphs.add_style_interactionsigns(local_igraph)
PyBoolNet.InteractionGraphs.igraph2image(local_igraph, "local_igraph.dot")


# %% state transition graph

updatescheme = "synchronous"
#updatescheme = "asynchronous"

PyBoolNet.StateTransitionGraphs.create_image(
    primes, updatescheme, f"stg_{updatescheme}.dot")


PyBoolNet.StateTransitionGraphs.create_image(
    primes,
    updatescheme,
    f"stg2_{updatescheme}.dot",
    #InitialStates={"L": 0, "P": 1, "B": 0},
    #Styles=["anonymous", "tendencies", "mintrapspaces"]
    Styles=["anonymous", "mintrapspaces"]
)


# %% Attractor :

stg = PyBoolNet.StateTransitionGraphs.primes2stg(primes, updatescheme)


nx.draw(stg)
pos = nx.spring_layout(stg)
plt.savefig("Graph.png", format="PNG")


primes = PyBoolNet.FileExchange.bnet2primes(bnet)
primes
stg = PyBoolNet.StateTransitionGraphs.primes2stg(primes, updatescheme)
steady, cyclic = PyBoolNet.Attractors.compute_attractors_tarjan(stg)
# A B L Le M P
print(steady)
print(cyclic)
len(cyclic)


# %%

# compute weak, strong and cycle-free basins

attrs = PyBoolNet.Attractors.compute_json(primes, updatescheme)
state = attrs["attractors"][0]["state"]["str"]
print(state)

weak = PyBoolNet.Basins.weak_basin(primes, updatescheme, state)
for key, value in weak.items():
    print("{} = {}".format(key, value))

strong = PyBoolNet.Basins.strong_basin(primes, updatescheme, state)
for key, value in strong.items():
    print("{} = {}".format(key, value))

cycfree = PyBoolNet.Basins.cyclefree_basin(primes, updatescheme, state)
for key, value in cycfree.items():
    print("{} = {}".format(key, value))


# to save the results and plot basins extend the attrs data

PyBoolNet.Basins.compute_basins(attrs)
PyBoolNet.Attractors.save_json(attrs, "attrs_basin.json")
PyBoolNet.Basins.create_barplot(attrs, "basin_barplot.pdf")
PyBoolNet.Basins.create_piechart(attrs, "basin_piechart.pdf")
