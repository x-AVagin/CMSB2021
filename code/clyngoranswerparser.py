
import collections

import pandas as pd

import sympy


def clyngorAnswer2dftimecourse(answer, wantedpredicate):
    """
    Recreated pandas.df timecourse from the given wantedpredicate of the shape (time, agent, value)

    Arguments:
    ----------
    wantedpredicate : clyngor answer
    predicate : predicate from which reconstruct the ts

    Returns :
    ---------
    pandas df storing the timecourse.
    one line per time
    one column per agent
    """

    df_datadict = collections.defaultdict(dict)
    # df_datadict[node][time] = value

    for predicate, arguments in answer:
        # print(predicate, arguments)
        if predicate == wantedpredicate:
            # print(predicate, arguments)
            time, node, value = arguments
            df_datadict[node][time] = value
    df_datadict
    df = pd.DataFrame(df_datadict)
    df.head()
    df.index.values
    df.empty
    if not df.empty:
        min(df.index.values)
        max(df.index.values)
        df = df.reindex(range(min(df.index.values), max(df.index.values)))

    return df


def clyngorAnswer_retrivebypredicate(answer, wantedpredicate, by_arity, number=None):
    """ Return the value(s) of a predicate present (or not) in a clyngor answer

    Argument :
    ----------
    - clyngor answer
    - wantedpredicate : string
    - by_arity : bool
    tells if solve().*by_arity* has been used.
    If True, answer is a dictionary with keys being predicate/arity
    If False, it is a frozenset of tuples (predicate, (tuple of values) )
    - number : int how many of such predicate should be present in the clyngor answer

    Return :
    --------
    list of tuples.
    each tuple corresponds to the arguments of a predicate of the shape wantedpredicate(arguments)
    """

    arg_list = []

    if by_arity:
        arg_list = answer[wantedpredicate]
    else:
        for predicate, arguments in answer:
            # print(predicate, arguments)
            if predicate == wantedpredicate:
                # print(predicate, arguments)
                arg_list.append(arguments)

    if number:
        assert(len(arg_list) == number)

    return arg_list


def clyngorAnswer2nbtime(answer, by_arity=True):
    """ Gives the numberoftimes in a clyngorAnswer

    Argument :
    ----------
    - clyngor answer
    - by_arity : bool
    tells if solve().*by_arity* has been used.
    If True, answer is a dictionary with keys being predicate/arity
    If False, it is a frozenset of tuples (predicate, (tuple of values) )

    Returns
    -------
    number of time value (int)
    """

    if by_arity:
        ((nbot, *_), *_) = answer["nbtime/1"]
        return nbot
    else:
        nbot_list = clyngorAnswer_retrivebypredicate(answer, "nbtime", 1)
        # unpack. oO (mae_list[0] is a tuple of 1 element)
        (nbot, *_) = nbot_list[0]
        return nbot


def clyngorAnswer2numberofcubes(answer, by_arity=True):
    """ Gives the number of cubes in a clyngorAnswer

    Argument :
    ----------
    - clyngor answer
    - by_arity : bool
    tells if solve().*by_arity* has been used.
    If True, answer is a dictionary with keys being predicate/arity
    If False, it is a frozenset of tuples (predicate, (tuple of values) )

    Returns
    -------
    number of cubes value (int)
    """

    if by_arity:
        ((noc, *_), *_) = answer["numberofcubes/1"]
        return noc
    else:
        noc_list = clyngorAnswer_retrivebypredicate(answer, "numberofcubes", 1)
        # unpack. oO (mae_list[0] is a tuple of 1 element)
        (noc, *_) = noc_list[0]
        return noc


def clyngorAnswer2mae(answer, by_arity=True):
    """ Gives the mae calculated by ASP (in the predicate "mae")

    Argument :
    ----------
    - clyngor answer
    - by_arity : bool
    tells if solve().*by_arity* has been used.
    If True, answer is a dictionary with keys being predicate/arity
    If False, it is a frozenset of tuples (predicate, (tuple of values) )

    Returns
    -------
    mae value (int)
    """

    # note that even if optimization is mae, there are cases where no predicate "mae" might have been generated.
    # Thus TODO : add test to avoid having an error while trying to retrieve the predicate "mae" that has not been generated
    # Cases where it will no be generated = cases where no predicate "error" has been generated
    # (would also work testing for the presence of the predicate pbDynamic_countrerexample, but we might not generate these predicate in the future,
    # on the contrary, we will always need the "error" predicate the the pipeline to generate the heatmaps. so lets take them instead)
    # TODO : unit test that an "mae" predicate generated <-> predicate "error" generated
    # TODO : unit test that no "mae" predicate generated <-> no predicates "error" generated

    if by_arity:
        ((mae, *_), *_) = answer["mae/1"]
        return mae
    else:
        mae_list = clyngorAnswer_retrivebypredicate(answer, "mae", 1)
        # unpack. oO (mae_list[0] is a tuple of 1 element)
        (mae, *_) = mae_list[0]
        return mae


def clyngorAnswer2errorvector(answer):
    """ Extract error abour x (predicate error/2)

    Argument :
    ----------
    clyngor answer

    Returns
    -------
    error list : the error associated at each time step
    """
    errors_list = clyngorAnswer_retrivebypredicate(answer, "error")
    len(errors_list)
    # error(time, errorvalue)
    # erors_list is a list of tuple (time, errorvalue)
    # sort the error by time (the first element of each tuple in the list)
    errors_list.sort()
    # keep only the errorvalue (time will be the index of each element)
    errors_list = [error for (_, error) in errors_list]
    len(errors_list)
    return errors_list


def answerDict2sympystring(answerdict):
    """
    Argument :
    -----------
    answer : *one* answer in its clauseDict shape.
    can get get from a call to clyngorAnswer2clauseDict(answer)
    keys are [clauseid] ["pos" / "neg"]
    val is the list of the element present in the answer.
        - "pos" -> positive litteral
        - "neg" -> negative litteral

    Return :
    --------
    a string that can be read by sympy for simpification process
    - AND = &
    - OR = |
    - NEGATION = ~


    Examples :
    -----------


    >>> answerdict = { 1 : {"pos" : {"A", "B"}, "neg" :  {"C"} },
                        2 : {"pos" : {"D"} , "neg" : {}}
                        }
    >>> answerDict2sympystring(answerdict)
    '(A&B&~C)|(D)'

    """

    if len(answerdict) == 1 and 0 in answerdict.keys():
        # the boolean expression is the function False (clauseID = 0)
        return ""

    clauses_strings = []
    for clauseID, clauseElements in answerdict.items():
        # print(clauseID, clauseElements)

        clause_elements_forsympy = []

        for element in clauseElements["pos"]:
            clause_elements_forsympy.append(element)
        for element in clauseElements["neg"]:
            clause_elements_forsympy.append(f"~{element}")

        clause_string = f"({'&'.join(clause_elements_forsympy)})"

        clauses_strings.append(clause_string)

    answer_string = "|".join(clause_string
                             for clause_string in clauses_strings)
    return answer_string


def clyngorAnswer2answerDict(answer, by_arity):
    """
    Arguments:
    ----------
    - answer : *one* answer from the results of clyngor.solve
    - by_arity : bool
    tells if solve().*by_arity* has been used.
    If True, answer is a dictionary with keys being predicate/arity
    If False, it is a frozenset of tuples (predicate, (tuple of values) )

    Return :
    ---------
    a dict constructed from the clause predicate present in the answer
    keys are clauseID , "pos" / "neg"
    answerDict[clauseID]["pos"] returns the set of positive litterals present in clause clauseID
    answerDict[clauseID]["neg"] returns the set of negative litterals present in clause clauseID
    If the returned dictionary is empty, it means that the component is constant.
    The value of the constant can be derived later looking at the predicate constant/1
    or directly at the values taken by the component in the binarized time serie.

    Example :
    ---------

    >>> answer = {"clauseID/1": [0], "constant/1": [1]}
    >>> clyngorAnswer2answerDict(answer, by_arity = True)
    {}
    >>> answer = [("clauseID", (0))]
    >>> clyngorAnswer2answerDict(answer, by_arity = False)
    {}

    >>> answer = {"clause/3" : [(1, "A", 1), (1, "B", 1), (1, "C", -1), (1, "D", 0), (2, "A", 0), (2, "B", 0), (2, "C", 0), (2, "D", 1)] }
    >>> clyngorAnswer2answerDict(answer, by_arity = True)
    {1: {'pos': {'A', 'B'}, 'neg': {'C'}}, 2: {'pos': {'D'}, 'neg': set()}}

    >>> answer = [
        ("clause", (1, "A", 1)),
        ("clause", (1, "B", 1)),
        ("clause", (1, "C", -1)),
        ("clause", (1, "D", 0)),
        ("clause", (2, "A", 0)),
        ("clause", (2, "B", 0)),
        ("clause", (2, "C", 0)),
        ("clause", (2, "D", 1))
    ]
    >>> clyngorAnswer2answerDict(answer, by_arity = False)
    {1: {'pos': {'A', 'B'}, 'neg': {'C'}}, 2: {'pos': {'D'}, 'neg': set()}}

    """

    # dict of dict of set accessed by [clauseid]["pos" / "neg"]. Returns a set
    toreturn = dict()

    if by_arity:
        # answer is a dict.
        # the keys of the dict are predicates/arity
        # the values of the dict are list of values taken by the predicate
        # answer is a frozern set of tuples (predicate, (tuple of values))

        if (0, ) in answer["clauseID"]:
            # either it has been written in the lp file because no parent,
            # or it has been chosen by the program
            # in any case, let's assert it is the only clause picked,
            # and return an empty dict
            assert(len(answer["clauseID"]) == 1)
            return dict()
            # if we were going in the next if block, we would end up returning
            # {0: {'pos' : set(), 'neg' : set()}} instead of a real empty dict

        # "clause/3" might not be in answer (if the node is a constant -> without parents)
        if "clause/3" in answer:
            for (clauseID, nodeID, val) in answer["clause/3"]:
                if not clauseID in toreturn:
                    toreturn[clauseID] = dict()
                    toreturn[clauseID]["pos"] = set()
                    toreturn[clauseID]["neg"] = set()
                if val != 0:  # the node is really in the clause :
                    toreturn[clauseID]["pos" if val ==
                                       1 else "neg"].add(nodeID)

    else:
        # answer is a frozern set of tuples (predicate, (tuple of values))
        # need to iterate over all to retrieve the tuples concerning predicate "clause"
        for e in answer:
            # print(e)
            predicate = e[0]
            if predicate == "clause":
                clauseID, nodeID, val = e[1]
                if clauseID == 0:
                    # either it has been written in the lp file because no parent,
                    # or it has been chosen by the program
                    # in any case, return an empty dict
                    return dict()
                    # if we continuing the for loop, we would end up returning
                    # {0: {'pos' : set(), 'neg' : set()}} instead of a real empty dict
                    # TODO : also assert "clauseID(0)" is the only clause that has been picked.
                if not clauseID in toreturn:
                    toreturn[clauseID] = dict()
                    toreturn[clauseID]["pos"] = set()
                    toreturn[clauseID]["neg"] = set()
                if val != 0:  # the node is really in the clause :
                    toreturn[clauseID]["pos" if val ==
                                       1 else "neg"].add(nodeID)

    return toreturn


def clyngorAnswer_determineConstantValue(answer, by_arity):
    """
    Arguments:
    ----------
    - answer : *one* answer from the results of clyngor.solve
    - by_arity : bool
    tells if solve().*by_arity* has been used.
    If True, answer is a dictionary with keys being predicate/arity
    If False, it is a frozenset of tuples (predicate, (tuple of values) )

    Return :
    ---------
    either sympy.true or sympy.false
    depending on if the component has been found to be
    True (predicate 'constant/1' is 1)
    or False (predicate 'constant/1' is -1)

    Example :
    ---------

    >>> answer = {"clauseID/1": [0], "constant/1": [1]}
    >>> clyngorAnswer_determineConstantValue(answer, by_arity = True)
    True

    >>> answer = [
        ("clauseID", (0)),
        ("constant", (1))
    ]
    >>> clyngorAnswer_determineConstantValue(answer, by_arity = False)
    True


    >>> answer = {"clauseID/1": [0], "constant/1": [-1]}
    >>> clyngorAnswer_determineConstantValue(answer, by_arity = True)
    False

    >>> answer = [
        ("clauseID", (0)),
        ("constant", (-1))
    ]
    >>> clyngorAnswer_determineConstantValue(answer, by_arity = False)
    False


    >>> answer = {"clause/3" : [(1, "A", 1), (1, "B", 1), (1, "C", -1), (1, "D", 0), (2, "A", 0), (2, "B", 0), (2, "C", 0), (2, "D", 1)] }
    >>> clyngorAnswer_determineConstantValue(answer, by_arity = True)
    -> abrupt quitting on error

    >>> answer = [
        ("clause", (1, "A", 1)),
        ("clause", (1, "B", 1)),
        ("clause", (1, "C", -1)),
        ("clause", (1, "D", 0)),
        ("clause", (2, "A", 0)),
        ("clause", (2, "B", 0)),
        ("clause", (2, "C", 0)),
        ("clause", (2, "D", 1))
    ]
    >>> clyngorAnswer_determineConstantValue(answer, by_arity = False)
    -> abrupt quitting on error

    """

    if by_arity:
        # answer is a dict.
        # the keys of the dict are predicates/arity
        # the values of the dict are list of values taken by the predicate
        # answer is a frozern set of tuples (predicate, (tuple of values))
        if 'constant/1' in answer:
            # there should be only one predicate 'constant/1', which contain the value of the constant component
            if len(answer['constant/1']) == 1:
                # weird unpacking the first element of a tuple of 1 element from a frozenset
                (constantValue,), = answer['constant/1']
                if constantValue == 1:
                    return sympy.true
                elif constantValue == -1:
                    return sympy.false
        # if constant not in answer OR if constant/1 has a different value that 1 or -1
        import pdb
        pdb.set_trace()
        print("problem : cannot determine the value of the constant, exit")
        exit(1)

    else:
        # answer is a frozern set of tuples (predicate, (tuple of values))
        # need to iterate over all to retrieve the tuples concerning predicate "clause"
        for e in answer:
            # print(e)
            predicate = e[0]
            if predicate == "constant":
                constantValue = e[1]
                if constantValue == 1:
                    return sympy.true
                elif constantValue == -1:
                    return sympy.false
        # if constant not in answer OR if constant/1 has a different value that 1 or -1
        import pdb
        pdb.set_trace()
        print("problem : cannot determine the value of the constant, exit")
        exit(1)


def clyngorAnswer2sympy(answer, by_arity):
    """
    Argument :
    -----------
    - answer : *one* answer from clyngor solve
    - by_arity : bool
    tells if solve().by_arity has been used.
    If True, answer is a dictionary with keys being predicate/arity
    If False, it is a frozenset of tuples (predicate, (tuple of values) )

    Return :
    --------
    a sympy version of the clygor answer

    Exaplanations :
    -------------

    the boolean function is encoded by a list of chosen clauses/3
    each clauses is : (clauseID, parentname, parentinfluence)
    parentinfluence can by either :
      0 -> no influence
      1 -> positive influence
     -1 -> negative influence.
    the clause is interpreted as a conjunction of its element that do not have an influence of 0.
    the complete boolean formula is the disjunction of the conjunctions.
        For example :
        clause(1, a, 1). clause(1, b, 1). clause (2, c, -1)
        is equivalent to :
        (a and b) or (not c)

    Example :
    ---------

    >>> answerClingo = {
    'clauseID/1': frozenset({(0)}),
    'constant/1': frozenset({(1)})
    }
    >>> answerSympy = clyngorAnswer2sympy(answerClingo, by_arity = True)
    >>> str(answerSympy)

    >>> answerClingo = [
        ("clauseID", (0)),
        ("constant", (1))
    ]
    >>> answerSympy = clyngorAnswer2sympy(answerClingo, by_arity = False)
    >>> str(answerSympy)
    ~C | (A & B)


    >>> answerClingo = {
    'clause': frozenset({(1, 'sbmlparamsnotconstant_v', 0), (1, 'sbmlparamsnotconstant_u', 1)}),
    ('clause', 3): frozenset({(1, 'sbmlparamsnotconstant_v', 0), (1, 'sbmlparamsnotconstant_u', 1)}),
    'clause/3': frozenset({(1, 'sbmlparamsnotconstant_v', 0), (1, 'sbmlparamsnotconstant_u', 1)})
    }
    >>> answerClingo["clause"]
    >>> answerClingo["clause", 3]
    >>> answerClingo["clause/3"]
    >>> answerSympy = clyngorAnswer2sympy(answer, by_arity = True)
    >>> str(answerSympy)

    >>> answerClingo = [
        ("clause", (1, "a", 1)),
        ("clause", (1, "b", 1),),
        ("clause", (1, "c", 0),),
        ("clause", (2, "a", 0)),
        ("clause", (2, "b", 0),),
        ("clause",  (2, "c", -1)),
    ]
    >>> answerSympy = clyngorAnswer2sympy(answerClingo, by_arity = False)
    >>> str(answerSympy)
    ~C | (A & B)

    """
    answerDict = clyngorAnswer2answerDict(answer, by_arity=by_arity)

    if not answerDict:
        # -> the component is constant (or at least not dependent of the given input)
        # and the value of the constant can be determined looking at the 'constant/1' predicate
        return clyngorAnswer_determineConstantValue(answer, by_arity=by_arity)

    answerSympyString = answerDict2sympystring(answerDict)
    if answerSympyString:
        answerSympy = sympy.sympify(answerSympyString)
        return answerSympy


def bfSympy2bfbnet(bfSympy):
    """
    Argument:
    ---------
    bfSympy: a boolean function in sympy

    Returns:
    --------
    string bfbnet:
    the same boolean function usable in the bnet format.

        - '0' encodes for 'False'
        - the negation from sympy (~) is replaced by '!'

    Examples :
    ---------
    >>> import sympy

    >>> bfSympy = sympy.false
    >>> bfSympy2bfbnet(bfSympy)
    '0'

    >>> bfSympy = sympy.sympify("(w & y & z) | (x & y & z) | (~w & ~y)")
    >>> bfSympy2bfbnet(bfSympy)
    '(w & y & z) | (x & y & z) | (!w & !y)'
    """

    print(bfSympy)
    if bfSympy == sympy.true:
        bfbnet = "1"
    elif bfSympy == sympy.false:
        bfbnet = "0"
    else:
        bfbnet = str(bfSympy)
        bfbnet = bfbnet.replace("~", "!")

    return bfbnet
