import pathlib
import random
import sys
import textwrap

import numpy as np
import pandas as pd

try:
    sys.path.insert(1, 'code/')
    import pknhandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd

random.seed(1)


def save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README):

    print(f"Creation of the test in {dirpath}")

    dirpath = pathlib.Path(dirpath)

    save_bnets(bnets, dirpath)

    siffilepath = dirpath / "pkn.sif"
    pknhandler.sifstruct2siffile(pkn, siffilepath)
    dotfilepath = dirpath / "pkn.dot"
    pknhandler.sifstruct2dotfile(pkn, dotfilepath)

    pddf_and_save(ts_data,
                  dirpath,
                  keys=ts_keys)
    saveREADME(
        text_README,
        dirpath
    )


def get_random_intvalues(start, end, num):
    """
    Arguments:
    -----------
    - start (int)
    - end (int)
    - num (int)

    Returns:
    --------
    An array of <num> integers ranging from <start> to <end>

    Example:
    --------
    >>> random.seed(1)
    >>> get_random_intvalues(0, 100, 10)
    [17, 72, 97, 8, 32, 15, 63, 57, 60, 83]

    """

    return random.sample(range(start, end), num)


def get_uncorrelated_intvalues(start, end, values, cor=0.001, seed=1):
    """
    Argument:
    --------
    - start (int)
    - end (int)
    - values (array of int)
    - cor (float - default 0.001) absolute value of the the correlation accepted between the 2 arrays.
    - seed (int) : which seed to use for the randomness

    Returns:
    -------
    An array of int of the same size than <values>.
    These 2 arrays are uncorelated (the absolute value of the 2 array is smaller than the given cor)

    Example:
    --------
    >>> random.seed(1)
    >>> values = [98, 55, 44, 3, 11, 66, 34, 93, 52, 19]
    >>> start = 0
    >>> end = 100
    >>> get_uncorrelated_intvalues(start, end, values)
    [94, 29, 50, 71, 51, 22, 61, 33, 78, 42]
    """
    random.seed(seed)

    correlation = 1
    num = len(values)
    while not abs(correlation) < cor:
        random_vals = get_random_intvalues(start, end, num)
        correlation = np.corrcoef(values, random_vals)[0, 1]
        # corrcoef returns a correlation matrix

    return random_vals


def get_increasing_intvalues(start, end, num):
    """
    Arguments:
    -----------

    - start (int)
    - end (int)
    - num (int)

    Returns:
    --------
    An array of <num> integers increasing from <start> to <end>

    Example:
    --------
    >>> random.seed(1)
    >>> get_increasing_intvalues(0, 100, 10)
    [8, 15, 17, 32, 57, 60, 63, 72, 83, 97]

    """

    return sorted(get_random_intvalues(start, end, num), reverse=False)


def get_decreasing_intvalues(start, end, num):
    """
    Arguments:
    -----------

    - start (int)
    - end (int)
    - num (int)

    Returns:
    --------
    An array of <num> integers decreasing from <start> to <end>

    Example:
    --------
    >>> random.seed(1)
    >>> get_decreasing_intvalues(0, 100, 10)
    [97, 83, 72, 63, 60, 57, 32, 17, 15, 8]

    """

    return sorted(get_random_intvalues(start, end, num), reverse=True)


def get_shifted(vals, shift):
    """

    Arguments:
    -----------

    - vals (array (of int))
    - shift (int)

    Returns:
    --------
    An array of composed of the values in <vals>, but shifted of <shift> indexes.
    The <shift> first shifted values are replaced by 0.

    Example:
    --------
    >>> get_shifted([1, 2, 3], 1)
    [0, 1, 2]
    >>> get_shifted([1, 2, 3], 2)
    [0, 0, 1]


    """

    ret = [0] * shift
    ret.extend(vals[:-shift])

    return ret


def noisify(vals, mean=0, std=1):
    """
    Arguments:
    ----------
    - vals (array (of int)): values to noisify
    - mean (float): mean of the noise
    - std (float): stg of the noise

    Returns:
    --------
    An array of noisified values.

    Examples:
    ---------
    >>> random.seed(1)
    >>> noisify([1, 1, 1], 0, 1)
    array([ 0.65634503, -0.12447565,  2.14670318])
    """

    noise = np.random.normal(mean, std, len(vals))
    noised = vals + noise

    return noised


def thresholdify(vals, threshold, use_min, use_max):
    """
    Arguments:
    ----------
    - vals (array (of int)): values to noisify
    - threshold (float): thresold to apply
    - use_min : if the values that are under the threshold should be mapped to the min value observed in the array.
    - use_max : if the values that are over the threshold should be mapped to the max value observed in the array.

    Returns:
    --------
    An array of thresholdify values:
    - in use_min set to True, all the values under the threshold will be equal to the minimal value observed in the given array of values
    - in use_max set to True, all the values over the threshold will be equal to the maximal value observed in the given array of values


    Examples:
    ---------
    >>> thresholdify([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1, 6], 3, use_min = True, use_max = True)
    [0, 0, 0, 0, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 0, 0, 0, 9]
    >>> thresholdify([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1, 6], 3, use_min = True, use_max = False)
    [0, 0, 0, 0, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 0, 0, 0, 6]
    >>> thresholdify([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1, 6], 3, use_min = False, use_max = True)
    [0, 1, 2, 3, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 3, 2, 1, 9]

    """

    # TODO :think about having a better code ¯\_(ツ)_/¯

    toret = []

    if use_max:
        max_ = max(vals)
    if use_min:
        min_ = min(vals)

    for v in vals:
        if v > threshold:
            if use_max:
                toret.append(max_)
            else:
                toret.append(v)
        else:  # v <= threshold
            if use_min:
                toret.append(min_)
            else:
                toret.append(v)

    return toret


def pddf_and_save(columns, dirpath, keys):
    """
    Create a pandas dataframe out of the data given.
    Name the columns using the given keys
    Save the dataframe as a csv in dirpath/ts.csv

    Arguments:
    ----------
    - columns (array of array (of float)) : each array corresponds to the data of a column.
    - dirpath (str) : path of the directory where the pd.df will be saved under the name "ts.csv"
    - keys (array of str) : name of the columns of the dataframe.

    Returns:
    --------
    - a dataframe constituded of the columns given in the <columns> parameter.
    whose names are from the <keys> parameter

    """
    assert (len(keys) == len(columns))

    location = pathlib.Path(dirpath)
    location /= "ts.csv"
    location.parent.mkdir(parents=True, exist_ok=True)

    df = pd.concat([pd.Series(col) for col in columns], axis=1, keys=keys)
    df.to_csv(location, index=True)

    return df


def saveREADME(text, dirpath):
    """
    It creates a readme.
    Information about the awaited bnets and pkn are automatically retrieved from bnets/*.bnet and pkn.sif files in the dirpath directory

    Arguments:
    ----------
    - txt (str) : explanation about how the ts was generated
    - dirpath (str) : path of the directory where the REAMDE will be saved
    """

    location = pathlib.Path(dirpath)

    bnets = ""
    location_bnets = location / 'bnets'
    for bnetfile in location_bnets.glob("*.bnet"):
        with open(bnetfile, 'r') as f:
            bnets += f.read()
        bnets += ("\n---\n")

    pkn = ""
    location_pkn = location / 'pkn.sif'
    with open(location_pkn, 'r') as f:
        pkn = f.read()

    location /= "README"
    with location.open("w") as f:
        f.write(
            f'''\
            Should return:
            ===============
            {bnets}

            Entries:
            ==========

            PKN:
            {pkn}


            ts :
            {text}
            ''')


def save_bnets(bnets, dirpath):
    """
    Arguments :
    -----------
    - bnets (array of str) : bnets to save
    - dirpath (str) : path of the directory where the pkn will be saved under the name "pkn.sif"
    """

    location = pathlib.Path(dirpath)
    location /= "bnets"
    location.mkdir(parents=True, exist_ok=True)

    bnets = [textwrap.dedent(bnet) for bnet in bnets]

    for i, bnet in enumerate(bnets):
        filepath = location / f"{i}.bnet"
        with filepath.open("w") as f:
            f.write(bnet)
