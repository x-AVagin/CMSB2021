import argparse
import sys
import textwrap

import networkx as nx
import numpy as np
import pandas as pd
import PyBoolNet

import mpbn

try:
    sys.path.insert(1, 'code/')
    import tshandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


def configstr2configmpbn(primes, configuration):
    """

    Arguments:
    ----------
    - primes : dict obtained from PyBoolNet.FileExchange.bnet2primes(bnetfile)
    - configuration : string = 0 / 1 vector. One bit for each agent sorted by name

    Return:
    -------
    A configuration usable in mpbn.
    It correspond to a dict.
    keys are agent name.
    value is an int corresponding to the state (0/1) of the agent in the given configuration.

    """

    assert(len(primes) == len(configuration))

    to_ret = dict()
    for prime, state in zip(sorted(primes), configuration):
        to_ret[prime] = int(state)

    return to_ret


if __name__ == "__main__":

    # %% parse arguments
    parser = argparse.ArgumentParser(
        description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bnetfile',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        bnet file containing the BN of which we want to find the STG
                        ''')
                        )
    parser.add_argument('--binarizedtscsv',
                        type=str,
                        required=False,
                        help=textwrap.dedent('''\
                        Optinal. csv of the binaried ts. If given, a path about the configurations is added in the sif. The edge type is "obs"
                        ''')
                        )

    parser.add_argument('--updatescheme',
                        type=str,
                        required=True,
                        choices=[
                            'synchronous',
                            'asynchronous',
                            'mixed',
                            'mergedsyncasync',
                            'mp',
                            'all'
                        ],
                        help=textwrap.dedent('''\
                        Update scheme that sould be used for the computation of the coverage
                        ''')
                        )

    parser.add_argument('--outputfile',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        path where the coverage file has to be saved
                        ''')
                        )

    # %% Retrieve arguments
    args = parser.parse_args()
    # sys.argv = ["--bnetfile", "FROM_CLUSTER/20201011_xp_minMAE_minInputs/bnet/BIOMD0000000116/1.bnet", "--binarizedtscsv", "test_ts_binarized.csv", "--outputfile", "test_coverage.txt"]
    # args = parser.parse_args(sys.argv)
    args.bnetfile
    args.binarizedtscsv
    args.outputfile

    # ----------
    # binarited ts to nx graph

    bts = tshandler.tsfile2pddf(args.binarizedtscsv)

    # do not remove consecutive duplicated lines
    # to not miss the self loops for the computation of the coverage.

    # assert columns are sorted in alphabetical order
    # so it matches the order of the agent in the configuration vectors in the stg.
    bts = bts.reindex(sorted(bts.columns), axis=1)
    # 0 is encoded by -1  in the binarized ts.
    # but pyboolnet and mpbn need zeros.
    bts.replace(-1, 0, inplace=True)

    # extract the state sequence as a list of string.
    # each string being a configuration
    obs = [''.join(map(str, row)) for row in bts.values]

    # If there is only one configuration o in the list obs,
    # perhaps the system was in its a fixed points.
    # Hence, add a seconde occurence of o to obs
    # in order to be able to see if there is a "transition" between o and itself
    # -> a self loop
    if len(obs) == 1:
        obs.extend(obs)

    # We now hove to check how many edges (o, o_) exist in the stgs :
    # for o, o_ in zip(obs, obs[1:]):
    #    print(o, o_)

    # ---
    coverage_all = {
        "synchronous": {
            "nb_config_full": 0,
            "nb_edges_full": 0,
            "nb_config_focused": 0,
            "nb_edges_focused": 0,
            "retreived_edges__number": 0,
            "retreived_edges__proportion": 0,
            "retreived_paths__number": 0,
            "retreived_paths__proportion": 0,
            "retreived_edges_all__number": 0,
            "retreived_edges_all__proportion": 0,
            "retreived_paths_all__number": 0,
            "retreived_paths_all__proportion": 0
        },
        "asynchronous": {
            "nb_config_full": 0,
            "nb_edges_full": 0,
            "nb_config_focused": 0,
            "nb_edges_focused": 0,
            "retreived_edges__number": 0,
            "retreived_edges__proportion": 0,
            "retreived_paths__number": 0,
            "retreived_paths__proportion": 0,
            "retreived_edges_all__number": 0,
            "retreived_edges_all__proportion": 0,
            "retreived_paths_all__number": 0,
            "retreived_paths_all__proportion": 0
        },
        "mixed": {
            "nb_config_full": 0,
            "nb_edges_full": 0,
            "nb_config_focused": 0,
            "nb_edges_focused": 0,
            "retreived_edges__number": 0,
            "retreived_edges__proportion": 0,
            "retreived_paths__number": 0,
            "retreived_paths__proportion": 0,
            "retreived_edges_all__number": 0,
            "retreived_edges_all__proportion": 0,
            "retreived_paths_all__number": 0,
            "retreived_paths_all__proportion": 0
        },
        "mergedsyncasync": {
            "nb_config_full": 0,
            "nb_edges_full": 0,
            "nb_config_focused": 0,
            "nb_edges_focused": 0,
            "retreived_edges__number": 0,
            "retreived_edges__proportion": 0,
            "retreived_paths__number": 0,
            "retreived_paths__proportion": 0,
            "retreived_edges_all__number": 0,
            "retreived_edges_all__proportion": 0,
            "retreived_paths_all__number": 0,
            "retreived_paths_all__proportion": 0
        },
        "mp": {
            "nb_config_full": 0,
            "retreived_paths__number": 0,
            "retreived_paths__proportion": 0,
            "retreived_paths_all__number": 0,
            "retreived_paths_all__proportion": 0

        }
    }

    if args.updatescheme == "all":
        coverage = coverage_all
    else:
        coverage = dict()
        coverage[args.updatescheme] = coverage_all[args.updatescheme]

    # - retreived_edges__number is the number of covered transition
    # - retreived_edges__proportion is the proportion of edge observed in the binarized ts that is also found in the stg obtained with a given semantic
    # = number of covered transition  / number of transition observed
    # At the begining, retreived_edges__proportion is 0 for all update scheme
    # - retrieved_paths__number
    # - retreived_paths__proportion is the proportion of edge observed in the binarized ts
    # among which there is a possible path in the stg obtained with a given semantic
    # At the begining, retreived_paths__proportion is 0 for all update scheme
    # - for the semantic mp, only the proportion of transition o, o'
    # such that o' is reachable from o
    # - Note that number of direct transitions observed is at maximal equal to len(obs) - 1
    transitionDirect_DONE = set()
    for o, o_ in zip(obs, obs[1:]):
        if o == o_ or (o, o_) in transitionDirect_DONE:
            continue
        else:
            transitionDirect_DONE.add((o, o_))
    if len(transitionDirect_DONE) == 0:
        print(
            " !!! no transitionDirect -> It seems all the components were constant for all the TS")

    # - The number of all possible arc is determined by the folowing :
    transitionAll_DONE = set()
    for i, o in enumerate(obs):
        for o_ in obs[i + 1:]:
            if o != o_ and (o, o_) not in transitionAll_DONE:
                transitionAll_DONE.add((o, o_))
    if len(transitionAll_DONE) == 0:
        print(
            " !!! no transitionAll -> It seems all the components were constant for all the TS")

    coverage["obs"] = {
        "nb_config_full": len(obs),
        "nb_config_focused": len(obs),
        "retreived_edges__number": len(transitionDirect_DONE) if len(transitionDirect_DONE) > 0 else np.nan,
        "retreived_paths__number": len(transitionDirect_DONE) if len(transitionDirect_DONE) > 0 else np.nan,
        "retreived_edges_all__number": len(transitionAll_DONE) if len(transitionAll_DONE) > 0 else np.nan,
        "retreived_paths_all__number": len(transitionAll_DONE) if len(transitionAll_DONE) > 0 else np.nan
    }

    print(f"""
          {len(obs)} configurations were observed in the binarized ts,
          {len(transitionDirect_DONE)} transitions observed,
          and {len(transitionAll_DONE)} combinaison of configurations at total.
          """)

    # ----------
    # coverage for synchronous / async case :

    # ---
    # Load the bnet file :
    primes = PyBoolNet.FileExchange.bnet2primes(args.bnetfile)
    primes

    # updatescheme = "asynchronous"
    for updatescheme in ("synchronous", "asynchronous", "mixed"):

        if not args.updatescheme == "all" and args.updatescheme != updatescheme:
            # to make the next steps, args.updatescheme has to be either equal to the current updatescheme considered or "all"
            continue

        if len(primes) > 10:
            print("do not compute STG because BN has more that 10 components, and this would require too much memory")
            continue

        if len(primes) == 0:
            stg_full = nx.DiGraph()
        else:
            stg_full = PyBoolNet.StateTransitionGraphs.primes2stg(
                primes,
                updatescheme
            )

        stg_full
        coverage[updatescheme]["nb_config_full"] = stg_full.number_of_nodes()
        coverage[updatescheme]["nb_edges_full"] = stg_full.number_of_edges()

        print("stats stg_full")
        print("nb nodes:", stg_full.number_of_nodes())
        print("nb_edges", stg_full.number_of_nodes())

        if len(primes) == 0:
            stg_focused = nx.DiGraph()
        else:
            stg_focused = PyBoolNet.StateTransitionGraphs.primes2stg(
                primes,
                updatescheme,
                InitialStates=obs
            )

        stg_focused

        coverage[updatescheme]["nb_config_focused"] = stg_focused.number_of_nodes()
        coverage[updatescheme]["nb_edges_focused"] = stg_focused.number_of_nodes()

        print("stats stg_focused")
        print("nb nodes:", stg_focused.number_of_nodes())
        print("nb edges", stg_focused.number_of_nodes())

        print(updatescheme)

        print("direct transition")
        transitionDirect_DONE = set()
        # direct transition
        for o, o_ in zip(obs, obs[1:]):

            if o == o_ or (o, o_) in transitionDirect_DONE:
                continue
            else:
                transitionDirect_DONE.add((o, o_))

            print("edge", o, o_)

            if stg_focused.has_edge(o, o_):
                print("had edge")
                coverage[updatescheme]["retreived_edges__number"] += 1

            if nx.has_path(stg_focused, o, o_):
                print("had path")
                coverage[updatescheme]["retreived_paths__number"] += 1

        print("all combi of transitions")
        # all combi of transition :
        transitionAll_DONE = set()
        for i, o in enumerate(obs):
            for o_ in obs[i + 1:]:
                if o == o_ or (o, o_) in transitionAll_DONE:
                    continue
                else:
                    transitionAll_DONE.add((o, o_))
                print("edge", o, o_)
                if stg_focused.has_edge(o, o_):
                    print("had edge")
                    coverage[updatescheme]["retreived_edges_all__number"] += 1

                if nx.has_path(stg_focused, o, o_):
                    print("had path")
                    coverage[updatescheme]["retreived_paths_all__number"] += 1

        coverage[updatescheme]
        len(obs)

        if len(transitionDirect_DONE) > 0:
            denominator = len(transitionDirect_DONE)
            coverage[updatescheme]["retreived_edges__proportion"] = coverage[updatescheme]["retreived_edges__number"] / denominator
            coverage[updatescheme]["retreived_paths__proportion"] = coverage[updatescheme]["retreived_paths__number"] / denominator
        else:  # if they were no transition to retreive, all the methods which returned a BN gets 1:
            coverage[updatescheme]["retreived_edges__proportion"] = 1
            coverage[updatescheme]["retreived_paths__proportion"] = 1

        if len(transitionAll_DONE) > 0:
            denominator = len(transitionAll_DONE)
            coverage[updatescheme]["retreived_edges_all__proportion"] = coverage[updatescheme]["retreived_edges_all__number"] / denominator
            coverage[updatescheme]["retreived_paths_all__proportion"] = coverage[updatescheme]["retreived_paths_all__number"] / denominator
        else:
            coverage[updatescheme]["retreived_edges_all__proportion"] = 1
            coverage[updatescheme]["retreived_paths_all__proportion"] = 1

        coverage[updatescheme]

    coverage

    # ----------
    # coverage for synchronous + async case :
    if args.updatescheme == "all" or args.updatescheme == "mergedsyncasync":
        updatescheme = "mergedsyncasync"
        # ---
        # Load the bnet file :
        primes = PyBoolNet.FileExchange.bnet2primes(args.bnetfile)
        primes

        if len(primes) > 10:
            pass
        else:
            # ---
            # stg full results of the merge of stg_sync_full and stg_async_full

            stg_sync_full = PyBoolNet.StateTransitionGraphs.primes2stg(
                primes,
                "synchronous"
            )
            stg_async_full = PyBoolNet.StateTransitionGraphs.primes2stg(
                primes,
                "asynchronous"
            )

            stg_full = nx.compose(stg_sync_full, stg_async_full)

            # ---
            assert(stg_sync_full.nodes() ==
                   stg_async_full.nodes() == stg_full.nodes())
            # len(stg_full.edges()) > len(stg_sync.edges()) + len(stg_async.edges())
            assert(
                len(stg_full.edges()) == len(stg_sync_full.edges())
                + len(stg_async_full.edges())
                - len(set(stg_sync_full.edges()
                          ).intersection(set(stg_async_full.edges())))
            )
            # ---

            coverage[updatescheme]["nb_config_full"] = stg_full.number_of_nodes()
            coverage[updatescheme]["nb_edges_full"] = stg_full.number_of_edges()

            print("stats stg_full")
            print("nb nodes:", stg_full.number_of_nodes())
            print("nb_edges", stg_full.number_of_nodes())

            # ---
            # stg focused results of the merge of stg_sync_focused and stg_async_focused

            stg_sync_focused = PyBoolNet.StateTransitionGraphs.primes2stg(
                primes,
                "synchronous"
            )
            stg_async_focused = PyBoolNet.StateTransitionGraphs.primes2stg(
                primes,
                "asynchronous"
            )

            stg_focused = nx.compose(stg_sync_focused, stg_async_focused)

            stg_focused

            coverage[updatescheme]["nb_config_focused"] = stg_focused.number_of_nodes()
            coverage[updatescheme]["nb_edges_focused"] = stg_focused.number_of_nodes()

            print("stats stg_focused")
            print("nb nodes:", stg_focused.number_of_nodes())
            print("nb edges", stg_focused.number_of_nodes())

            # ---
            print(updatescheme)

            print("direct transition")
            transitionDirect_DONE = set()
            # direct transition
            for o, o_ in zip(obs, obs[1:]):
                if o == o_ or (o, o_) in transitionDirect_DONE:
                    continue
                else:
                    transitionDirect_DONE.add((o, o_))

                print("edge", o, o_)

                if stg_focused.has_edge(o, o_):
                    print("had edge")
                    coverage[updatescheme]["retreived_edges__number"] += 1

                if nx.has_path(stg_focused, o, o_):
                    print("had path")
                    coverage[updatescheme]["retreived_paths__number"] += 1

            print("all combi of transitions")
            # all combi of transition :
            transitionAll_DONE = set()
            for i, o in enumerate(obs):
                for o_ in obs[i + 1:]:
                    if o == o_ or (o, o_) in transitionAll_DONE:
                        continue
                    else:
                        transitionAll_DONE.add((o, o_))
                    print("edge", o, o_)
                    if stg_focused.has_edge(o, o_):
                        print("had edge")
                        coverage[updatescheme]["retreived_edges_all__number"] += 1

                    if nx.has_path(stg_focused, o, o_):
                        print("had path")
                        coverage[updatescheme]["retreived_paths_all__number"] += 1

            coverage[updatescheme]
            len(obs)

            if len(transitionDirect_DONE) > 0:
                denominator = len(transitionDirect_DONE)
                coverage[updatescheme]["retreived_edges__proportion"] = coverage[updatescheme]["retreived_edges__number"] / denominator
                coverage[updatescheme]["retreived_paths__proportion"] = coverage[updatescheme]["retreived_paths__number"] / denominator
            else:
                coverage[updatescheme]["retreived_edges__proportion"] = 1
                coverage[updatescheme]["retreived_paths__proportion"] = 1

            if len(transitionAll_DONE) > 0:
                denominator = len(transitionAll_DONE)
                coverage[updatescheme]["retreived_edges_all__proportion"] = coverage[updatescheme]["retreived_edges_all__number"] / denominator
                coverage[updatescheme]["retreived_paths_all__proportion"] = coverage[updatescheme]["retreived_paths_all__number"] / denominator
            else:
                coverage[updatescheme]["retreived_edges_all__proportion"] = 1
                coverage[updatescheme]["retreived_paths_all__proportion"] = 1

            coverage[updatescheme]

            coverage

    # ----------
    # coverage for mp case :
    if args.updatescheme == "all" or args.updatescheme == "mp":
        coverage["mp"]["retreived_paths__number"] = 0
        coverage["mp"]["retreived_paths_all__number"] = 0

        try:
            mbn = mpbn.load(args.bnetfile)

            # direct transition
            transitionDirect_DONE = set()
            for o, o_ in zip(obs, obs[1:]):
                if o == o_ or (o, o_) in transitionDirect_DONE:
                    continue
                else:
                    transitionDirect_DONE.add((o, o_))
                print("edge", o, o_)

                if mbn.reachability(configstr2configmpbn(primes, o), configstr2configmpbn(primes, o_)):
                    print("o_ reachable from o")
                    coverage["mp"]["retreived_paths__number"] += 1

            # all possible transition :
            transitionAll_DONE = set()
            for i, o in enumerate(obs):
                for o_ in obs[i + 1:]:
                    if o == o_ or (o, o_) in transitionAll_DONE:
                        continue
                    else:
                        transitionAll_DONE.add((o, o_))
                    if mbn.reachability(configstr2configmpbn(primes, o), configstr2configmpbn(primes, o_)):
                        print("o_ reachable from o")
                        coverage["mp"]["retreived_paths_all__number"] += 1

            assert(len(transitionAll_DONE) == nb_combi)
            if len(transitionDirect_DONE) > 0:
                denominator = len(transitionDirect_DONE)
                coverage[updatescheme]["retreived_paths__proportion"] = coverage[updatescheme]["retreived_paths__number"] / denominator
            else:
                coverage[updatescheme]["retreived_paths__proportion"] = 1

            if len(transitionAll_DONE) > 0:
                denominator = len(transitionAll_DONE)
                coverage[updatescheme]["retreived_paths_all__proportion"] = coverage[updatescheme]["retreived_paths_all__number"] / denominator
            else:
                coverage[updatescheme]["retreived_paths_all__proportion"] = 1
        except:
            # because of asser_monotonicity, the expression forming the update function can not contain literals with both signs.
            print("Error while running the BN with MP semantics.")
            coverage["mp"]["retreived_paths__number"] = np.nan
            coverage["mp"]["retreived_paths__proportion"] = np.nan
            coverage["mp"]["retreived_paths_all__number"] = np.nan
            coverage["mp"]["retreived_paths_all__proportion"] = np.nan

    coverage

    coverage_table = pd.DataFrame(coverage)
    coverage_table

    coverage_table.to_csv(args.outputfile)
    print(f"coverage results have been save in {args.outputfile}")
