import argparse
import sys
import textwrap

try:
    sys.path.insert(1, 'code/')
    import ts2plots
    import tshandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":

    # %% help message
    parser = argparse.ArgumentParser(
        description="""
        Produces a stretched version of the given ts.
        At each time step, the agents of the system have a stretched value V ranging from -100 to 100.
        We use minmaxscaler to archive this.
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--tsfilepath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        csv file storing the result of the copasi simulation of the sbml file being processed
                        ''')
                        )
    parser.add_argument('--outfilepath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        path to where the csv file containing the stretched ts has to be written
                        ''')
                        )

    parser.add_argument("--globally",
                        type=str2bool,
                        nargs='?',
                        const=True,
                        required=True,
                        help=textwrap.dedent('''\
                            Whether to use the same strtching for all the components or not.
                            Default = False -> the stretching is done component wise, resulting in having a different binarization treshold for all the component in the binarisation step.
                        ''')
                        )

    args = parser.parse_args()

    # -----------
    ts = tshandler.tsfile2pddf(args.tsfilepath)
    # timecourse = tshandler.tsfile2pddf('data/test/BIOMD0000000111_res-simu.csv')
    # ts = tshandler.tsfile2pddf('experiments/xpname/ts/raf_synchronous_noise0.1_seed42_ts.csv')

    stretchedts = tshandler.ts2stretchedts(
        ts,
        minstretched=-100,
        maxstretched=100,
        globally=args.globally
    )

    stretchedts.to_csv(args.outfilepath)
