import re

# Note :
# -----
#
# sympy is not capable to tell that 2 minDNF of the same functions are the same
# Example :
# (A & ~C) | (C & ~A) | (~A & ~B) is already minimal
# but sympy computes : (A & ~C) | (C & ~A) | (~B & ~C)
# sympy cannot tell they are the same.
#
# Let's go back to the root of what a minDNF is.
#
# -1.  A min DNF is a DNF that is of minimal size among all the equivalent DNF.
# size = number of occurence of litterals.
# -2.  in here :  https://pdfs.semanticscholar.org/d305/f0f295f4023a1d44981cffff1c178fa7bbb0.pdf
# they say that it is a cover that minimize a cost function
# and that this cost function is usually defined as the number of cubes.
#
# Let's take the first definition...


def get_litterals(expr):
    """
    Get a list of litterals present in a string shaped for sympy

    Argument:
    ---------
    expr : a string shaped for sympy

    Returns:
    --------
    int : the number of cubes

    Example :
    ---------
    >>> get_litterals(" a| (b & c)")
    ['a', 'b', 'c']
    """
    delimiters = "|", "&", "(", ")", " "
    expr
    regexPattern = '|'.join(map(re.escape, delimiters))
    regexPattern
    return list(filter(None, re.split(regexPattern, expr)))


def count_litterals(expr):
    """
    Count the number of litterals present in a string shaped for sympy

    Argument:
    ---------
    expr : a string shaped for sympy

    Returns:
    --------
    int : the number of cubes

    Example :
    ---------
    >>> count_litterals("a & b &c | (d & e)")
    5
    """
    return len(get_litterals(expr))


def count_cubes(expr):
    """
    Count the number of cube in a string shaped for sympy / in a sympy boolean expression object

    Argument:
    ---------
    expr : a string shaped for sympy

    Returns:
    --------
    int : the number of cubes

    Example :
    ---------
    >>> count_cubes(" a| (b & c)")
    2
    >>> count_cubes(sympy.sympify(" a| (b & c)"))
    2
    """
    try:
        return len(expr.args)
    except AttributeError:
        return expr.count("|") + 1
