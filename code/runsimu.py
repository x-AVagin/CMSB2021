# -*- coding: utf-8 -*-

# Modified by Athénaïs Vaginay

# Copyright (C) 2017 - 2018 by Pedro Mendes, Virginia Tech Intellectual
# Properties, Inc., University of Heidelberg, and University of
# of Connecticut School of Medicine.
# All rights reserved.

# Copyright (C) 2010 - 2016 by Pedro Mendes, Virginia Tech Intellectual
# Properties, Inc., University of Heidelberg, and The University
# of Manchester.
# All rights reserved.

# Copyright (C) 2009 by Pedro Mendes, Virginia Tech Intellectual
# Properties, Inc., EML Research, gGmbH, University of Heidelberg,
# and The University of Manchester.
# All rights reserved.


# This is an example on how to import an sbml file
# create a report for a time course simulation
# and run a time course simulation
#

import logging
import os
import sys

import yaml

import COPASI

try:
    sys.path.insert(1, 'code/')
    import logger
    import sbmlparser
    logger.set_loggers()
except ImportError:
    print("is it the correct working directory ?")
    # %pwd
logger = logging.getLogger('logger.runsimu')
logger.info("in runsimu")

# help(COPASI)


# create a datamodel
try:
    dataModel = COPASI.CRootContainer.addDatamodel()
except:
    dataModel = COPASI.CCopasiRootContainer.addDatamodel()

# args = ["BIOMD0000000111_url.xml"]
# args = ["/home/nanis/Documents/TRAVAIL/THESE/PROJETS/202006_Biomodels2BN/dataset/BioModels_Database-r31_pub-sbml_files/curated/BIOMD0000000111.xml"]
# args = ["/home/nanis/Documents/TRAVAIL/THESE/PROJETS/202006_Biomodels2BN/dataset/BioModels_Database-r31_pub-sbml_files/curated/BIOMD0000000001.xml"]
# args = ["data/BioModels/BioModels_Database-r31_pub-sbml_files/test/BIOMD0000000065.xml"]


def main(args):
    logger.info(f"Given file : {args}")

    # the two arguments to the main routine should be :
    # - the name of an SBML file
    # - the filepath to a yaml file giving information about the nodes taken into account.
    if len(args) != 2:
        logger.error("Usage is : python3 runsimu.py SBMLFILE TASKDESCRFILE\n")
        exit(1)

    sbmlfilename = args[0]
    taskdescr_filename = args[1]

    try:
        # load the model
        if not dataModel.importSBML(sbmlfilename):
            logger.error("Couldn't load {0}:".format(sbmlfilename))
            logger.error(COPASI.CCopasiMessage.getAllMessageText())
    except:
        logger.exeption(
            "Error while importing the model from file named \"" + sbmlfilename + "\".\n")
        exit(1)

    model = dataModel.getModel()
    assert model is not None

    print_model_statistics(model)

    # get the trajectory task object
    trajectoryTask = dataModel.getTask("Time-Course")
    assert (isinstance(trajectoryTask, COPASI.CTrajectoryTask))

    # help(trajectoryTask)

    # run a deterministic time course
    trajectoryTask.getMethod()
    trajectoryTask.setMethodType(COPASI.CTaskEnum.Method_deterministic)

    # activate the task so that it will be run when the model is saved
    # and passed to CopasiSE
    trajectoryTask.setScheduled(True)

    # create a new report that captures the time course result
    report = create_report(model, sbmlfilename, taskdescr_filename)
    # set the report for the task
    trajectoryTask.getReport().setReportDefinition(report)
    # set the output filename
    os.path.basename(sbmlfilename)
    trajectoryTask.getReport().setTarget(
        f"{os.path.splitext(os.path.basename(sbmlfilename))[0]}_ts.csv")
    # don't append output if the file exists, but overwrite the file
    trajectoryTask.getReport().setAppend(False)

    # get the problem for the task to set some parameters
    problem = trajectoryTask.getProblem()
    assert (isinstance(problem, COPASI.CTrajectoryProblem))

    # help(problem)

    # start at time 0
    dataModel.getModel().getInitialTime()
    dataModel.getModel().setInitialTime(0.0)

    # -----

    # step size :
    problem.getStepSize()
    problem.setStepSize(1)

    # duration of the simulation :
    # DEFAULT = simulate a duration of 1 time unit
    problem.getDuration()

    with open(taskdescr_filename, "r") as td_f:
        task_descr = yaml.load(td_f, Loader=yaml.FullLoader)

    problem.setDuration(task_descr["time_simu"])

    # Number of steps are automatically adjusted :
    problem.getStepNumber()
    # problem.setStepNumber(100) -> simulate 100 steps

    # -----

    # tell the problem to actually generate time series data
    problem.setTimeSeriesRequested(True)
    # tell the problem, that the simulation steps are automatically controlled
    problem.getAutomaticStepSize()
    problem.setAutomaticStepSize(True)
    # tell the problem, that we don't want additional output points for event assignments
    problem.getOutputEvent()
    problem.setOutputEvent(False)

    # set some parameters for the LSODA method through the method
    method = trajectoryTask.getMethod()

    # help(method)
    logger.info(f"Method={method}")

    parameter = method.getParameter("Absolute Tolerance")
    assert parameter is not None
    # help(parameter)
    logger.debug(f"is default: {parameter.isDefault()}")
    logger.debug(f"type : {parameter.getType()}")
    # print("value", parameter.getValue()) # Throw error in updated Copasi Version :
    # AttributeError: type object 'CCopasiParameter' has no attribute 'DOUBLE'
    logger.debug(f"UDblValue {parameter.getUDblValue()}")
    logger.debug(f"DblValue {parameter.getDblValue()}")

    logger.debug(COPASI.CCopasiParameter)
    logger.debug(dir(COPASI.CCopasiParameter))
    logger.debug(COPASI.CCopasiParameter.Type_DOUBLE)
    logger.debug(COPASI.CCopasiParameter.Type_UDOUBLE)

    assert parameter.getType() == COPASI.CCopasiParameter.Type_UDOUBLE
    parameter.setValue(1.0e-12)

    logger.info("COPASI task has been created")

    try:
        # now we run the actual trajectory
        result = trajectoryTask.process(True)
    except:
        logger.error("Error. Running the time course simulation failed.\n")
        # check if there are additional error messages
        if COPASI.CCopasiMessage.size() > 0:
            # print the messages in chronological order
            logger.error(COPASI.CCopasiMessage.getAllMessageText(True))
        exit(1)
    if not result:
        sys.stderr.write("Error. Running the time course simulation failed.\n")
        # check if there are additional error messages
        if COPASI.CCopasiMessage.size() > 0:
            # print the messages in chronological order
            logger.error(COPASI.CCopasiMessage.getAllMessageText(True))
        exit(1)

    logger.info("printing the results")
    # look at the timeseries
    print_results(trajectoryTask)


def print_model_statistics(model):
    logger.info("Model statistics for model \"" +
                model.getObjectName() + "\".")

    # output number and names of all compartments
    iMax = model.getCompartments().size()
    logger.info(f"Number of Compartments: {iMax}")
    logger.debug("Compartments: ")
    for i in range(0, iMax):
        compartment = model.getCompartment(i)
        assert compartment is not None
        logger.debug(f"\t{compartment.getObjectName()}")

    # output number and names of all metabolites
    iMax = model.getMetabolites().size()
    logger.info(f"Number of Metabolites: {iMax}")
    logger.debug("Metabolites: ")
    for i in range(0, iMax):
        metab = model.getMetabolite(i)
        assert metab is not None
        logger.debug("\t" + metab.getObjectName())

    # output number and names of all values
    iMax = model.getModelValues().size()
    logger.info(f"Number of Values: {iMax}")
    logger.debug("Values: ")
    for i in range(0, iMax):
        value = model.getModelValue(i)
        assert value is not None
        logger.debug(f"\t {value.getObjectName()}")

    # output number and names of all reactions
    iMax = model.getReactions().size()
    logger.info(f"Number of Reactions: {iMax}")
    logger.debug("Reactions: ")
    for i in range(0, iMax):
        reaction = model.getReaction(i)
        assert reaction is not None
        logger.debug(f"\t {reaction.getObjectName()}")


def print_results(trajectoryTask):
    timeSeries = trajectoryTask.getTimeSeries()
    # we simulated n steps, including the initial state, this should be
    # n = 1 step in the timeseries
    # assert timeSeries.getRecordedSteps() == n+1
    logger.info(
        f"The time series consists of {timeSeries.getRecordedSteps()} steps.")
    logger.info(
        f"Each step contains {timeSeries.getNumVariables()} variables.")
    logger.info("\nThe final state is: ")
    iMax = timeSeries.getNumVariables()
    lastIndex = timeSeries.getRecordedSteps() - 1
    for i in range(0, iMax):
        # here we get the particle number (at least for the species)
        # the unit of the other variables may not be particle numbers
        # the concentration data can be acquired with getConcentrationData
        logger.info(
            f"  {timeSeries.getTitle(i)}: {timeSeries.getData(lastIndex, i)}")
    # the CTimeSeries class now has some new methods to get all variable titles
    # as a python list (getTitles())
    # and methods to get the complete time course data for a certain variable based on
    # the variables index or the corresponding model object.
    # E.g. to get the particle numbers of the second variable as a python list
    # you can use getDataForIndex(1) and to get the concentration data you use
    # getConcentrationDataForIndex(1)
    # To get the complete particle number data for the second metabolite of the model
    # you can use getDataForObject(model.getMetabolite(1)) and to get the concentration
    # data you use getConcentrationDataForObject.
    # print (timeSeries.getTitles())
    # print (timeSeries.getDataForIndex(1))
    # print (timeSeries.getDataForObject(model))


def create_report(modelCOPASI, sbmlfilepath, taskdescr_filename):
    # create a report with the correct filename and all the species against
    # time.
    reports = dataModel.getReportDefinitionList()
    # create a report definition object
    report = reports.createReportDefinition("Report", "Output for timecourse")
    # set the task type for the report definition to timecourse
    report.setTaskType(COPASI.CTaskEnum.Task_timeCourse)
    # we don't want a table
    report.setIsTable(False)
    # the entries in the output should be separated by a ", "
    report.setSeparator(COPASI.CCopasiReportSeparator(", "))
    # we need a handle to the header and the body
    # the header will display the ids of the metabolites and "time" for
    # the first column
    # the body will contain the actual timecourse data
    header = report.getHeaderAddr()
    logger.info(f"header : {header}")
    body = report.getBodyAddr()
    logger.info(f"body : {body}")
    body.push_back(
        COPASI.CRegisteredCommonName(COPASI.CCommonName(dataModel.getModel().getCN().getString() + ",Reference=Time").getString()))
    body.push_back(COPASI.CRegisteredCommonName(
        report.getSeparator().getCN().getString()))
    header.push_back(COPASI.CRegisteredCommonName(
        COPASI.CDataString("time").getCN().getString()))
    header.push_back(COPASI.CRegisteredCommonName(
        report.getSeparator().getCN().getString()))

    # ----------
    # I used to take into account concentration / values of entities (species (metabolites) and  params (values))
    # that are not constant.
    # to spot them, i used to check whether or not the status of the entity was FIXED
    #   entity.getStatus() != COPASI.CModelEntity.Status_FIXED
    # but see my question and the explanation of Frank Bergmann and Stefan Hoops on the google group of copasi :
    # https://groups.google.com/g/copasi-user-forum/c/LdNFeByFqGc
    #
    # Hence the following update (idea of Frank Bergmann):
    # since we are starting with an sbml model, i already know the id's of elements i care about.
    # and we can just select the model elements by querying their sbml ids.

    with open(taskdescr_filename, "r") as td_f:
        task_descr = yaml.load(td_f, Loader=yaml.FullLoader)

    modelSBML = sbmlparser.sbmlfile2libsbmlmodel(sbmlfilepath)

    logger.error(task_descr['nodesTypes'])
    assert(all([nodesType in ["SBMLspecies", "SBMLparamsnotconstant"]
                for nodesType in task_descr['nodesTypes']]))

    listOfSpecies_toTake = []
    if "SBMLspecies" in task_descr['nodesTypes']:
        listOfSpecies_toTake = sbmlparser.getListOfNodes(
            modelSBML, ["SBMLspecies"], safe=False)

    listOfParams_toTake = []
    if "SBMLparamsnotconstant" in task_descr['nodesTypes']:
        listOfParams_toTake = sbmlparser.getListOfNodes(
            modelSBML, ["SBMLparamsnotconstant"], safe=False)

    numNodes_toTake = len(listOfSpecies_toTake) + len(listOfParams_toTake)
    logger.info(f"""
                {numNodes_toTake} columns have to be taken into account.
                They correspond to
                 {len(listOfSpecies_toTake)} metabolites: {listOfSpecies_toTake}:
                 and {len(listOfParams_toTake)} values: {listOfParams_toTake}
                """)

    # Note :getObjectCN returns a string whereas getCN returns a CCommonName

    # Note about status : (see table cheatsheet)
    # COPASI.CModelEntity.Status_FIXED = 0
    # COPASI.CModelEntity.Status_ASSIGNMENT = 1
    # COPASI.CModelEntity.Status_REACTIONS = 2
    # COPASI.CModelEntity.Status_ODE = 3
    # COPASI.CModelEntity.Status_TIME = 4

    # we now have the list of the sbml ids we take into account.
    # They are in the list columns_taken
    #   -> We now process each sbml id from the list and retrive the associated copasi element to add it in the body and header of the report.
    #   -> We are carefull to put a coma only if the current column is not the last one.
    #   we use the var len(columnsProcessed) in order to keep track of the number of col that has been preocessed

    # keep track of the nb of col that has been processed
    # if len(columnsProcessed) != numNodes_toTake - 1,
    # add a separator
    # TODO : think about using <(= ?) instead of !=...
    columnsProcessed = []

    for metab in modelCOPASI.getMetabolites():
        logger.info(f"processing metab {metab.getSBMLId()}")
        if (metab.getSBMLId() in listOfSpecies_toTake):
            logger.info(
                f"metab {metab} is part of the col we take into account")
            # add the concentration in the body
            # (alternatively, we could use "Reference=Amount" to get the particle number)
            metab.getObject(COPASI.CCommonName("Reference=Concentration"))
            metab.getObject(COPASI.CCommonName(
                "Reference=Concentration")).getCN()
            metab.getObject(COPASI.CCommonName(
                "Reference=Concentration")).getCN().getString()
            body.push_back(
                COPASI.CRegisteredCommonName(metab.getObject(COPASI.CCommonName("Reference=Concentration")).getCN().getString()))
            # add the corresponding id to the header
            # header.push_back(COPASI.CRegisteredCommonName(
            #     COPASI.CDataString(metab.getSBMLId()).getCN().getString()))
            sid = sbmlparser.get_safeNodeID(metab.getSBMLId(), "SBMLspecies")
            header.push_back(COPASI.CRegisteredCommonName(
                COPASI.CDataString(sid).getCN().getString()))
            # after each entry (except the last), we need a separator
            if len(columnsProcessed) < numNodes_toTake - 1:
                logger.info(
                    f"here (in metab), {len(columnsProcessed)} was not considered as the last column {numNodes_toTake - 1}")
                body.push_back(COPASI.CRegisteredCommonName(
                    report.getSeparator().getCN().getString()))
                header.push_back(COPASI.CRegisteredCommonName(
                    report.getSeparator().getCN().getString()))
            else:
                logger.info("last column, so do not add a separator")
            # one more col has been processed:
            columnsProcessed.append(metab.getSBMLId())

    for value in modelCOPASI.getModelValues():
        logger.info(f"processing value {value.getSBMLId()}")
        if (value.getSBMLId() in listOfParams_toTake):
            logger.info(
                f"value {value} is part of the col we take into account")
            # add to the body
            # use "Reference=Value" :
            # https://github.com/copasi/COPASI/blob/b61a53a782f533a487d6fbf3b90786cb1431e74e/copasi/bindings/java/unittests/Test_RunScan.java#L161
            value.getObject(COPASI.CCommonName("Reference=Value"))
            value.getObject(COPASI.CCommonName("Reference=Value")).getCN()
            value.getObject(COPASI.CCommonName(
                "Reference=Value")).getCN().getString()
            COPASI.CRegisteredCommonName(value.getObject(
                COPASI.CCommonName("Reference=Value")).getCN().getString())
            body.push_back(
                COPASI.CRegisteredCommonName(value.getObject(COPASI.CCommonName("Reference=Value")).getCN().getString()))
            # add to the header
            # header.push_back(COPASI.CRegisteredCommonName(
            #     COPASI.CDataString(value.getSBMLId()).getCN().getString()))
            logger.info("sid retrieved")
            sid = sbmlparser.get_safeNodeID(
                value.getSBMLId(), "SBMLparamsnotconstant")
            logger.info(sid)
            logger.info("pushing")
            header.push_back(COPASI.CRegisteredCommonName(
                COPASI.CDataString(sid).getCN().getString()))
            # after each entry (except the last), we need a separator
            if len(columnsProcessed) < numNodes_toTake - 1:
                logger.info(
                    f"here (in values), {len(columnsProcessed)} was not considered as the last column {numNodes_toTake - 1}")
                body.push_back(COPASI.CRegisteredCommonName(
                    report.getSeparator().getCN().getString()))
                header.push_back(COPASI.CRegisteredCommonName(
                    report.getSeparator().getCN().getString()))
            else:
                logger.info("last column, so do not add a separator")
            # one more col has been processed:
            columnsProcessed.append(value.getSBMLId())

    # Assert all the wanted columns have been processed:
    try:
        assert(len(columnsProcessed) == numNodes_toTake)
    except:
        logger.error("Missing some nodes in the columns of the ts file")
        logger.error(f"List of columns : {columnsProcessed}")
        logger.error(
            f"List of wanted nodes : species / {listOfSpecies_toTake} ; params : {listOfParams_toTake}")
        sys.exit(1)

    return report


if __name__ == '__main__':
    main(sys.argv[1:])  # the argument of index 0 is the script name
    logger.debug("end runsimu.py")
