import argparse
import os
import pathlib
import sys

import matplotlib.pyplot as plt
import pandas as pd

try:
    sys.path.insert(1, 'code/')
    import sbmlparser
    import tshandler
    from reports import Report

except ImportError:
    print("is it the correct working directory ?")
    exit(1)
    # %pwd


if __name__ == '__main__':

    rep = Report()

    parser = argparse.ArgumentParser(
        description='Description',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument('--sbmlList',
                        help='Path to the file listing the BIOMDS sbml files taken into account',
                        type=str,
                        required=True
                        )
    parser.add_argument('--xprespath',
                        help='Path to where the xp results are stored',
                        type=pathlib.Path,
                        required=True
                        )
    parser.add_argument('--outfolderpath',
                        help='Path to the folder where the report have to be saved. The images will be it the subfolder "img"',
                        type=str,
                        required=True
                        )

    args = parser.parse_args()
    # sys.argv = ["--xprespath", "experiments/xpname"]
    # args = parser.parse_args(sys.argv)

    # Here is where all the curation images that have been scraped from the Biomodels website are stored
    # image name : <BIOMOID>_curationimg.jpeg
    folder_curationImages = pathlib.Path(
        "SAVE_report_curation-allBIOMODELScurated/img")
    assert(folder_curationImages.exists() and folder_curationImages.is_dir())

    processedSBMLs = []

    with open(args.sbmlList, 'r') as f:
        for line in f:
            processedSBMLs.append(line.strip())

    curation_df = pd.read_excel(
        "table_curatedBIOMD_time-curation-ts.ods",
        index_col=0,  # BIOMDID
        engine="odf")

    # ---
    # Data used for plots later :
    data_nbconfigtotal = []
    data_nbconfigobserved = []
    # ---

    for processedSBML in processedSBMLs:

        rep.add_markdown("---------------------------")
        rep.add_markdown(processedSBML)

        BIOMD_id = os.path.splitext(os.path.basename(processedSBML))[0]

        # ----------
        # info manually retrieved from the curation images:
        rep.add_pddf(curation_df.loc[[BIOMD_id], :])

        # ----------
        # curation images:
        rep.add_external_figure(
            (folder_curationImages / f"{BIOMD_id}_curationimg.jpeg").resolve())
        # ----------
        # row ts generated from the pipeline:
        rep.add_external_figure((args.xprespath / "ts" /
                                 f"{BIOMD_id}_tsraw.png").resolve())
        # ----------
        # nb of binarized configurations in the binarized ts VS nb of total number of binarized configurations the system can adopted:

        # open the binarized ts :
        bts = tshandler.tsfile2pddf(
            args.xprespath / "ts" / f"{BIOMD_id}_binarizedtsts.csv")

        # remove duplicated lines to get the number of configation that were met
        bts_dedup = bts.drop_duplicates(inplace=False)

        # remove consecutive duplicated to get the transition that were observed
        # Code to remove consecutive duplicates from : https://stackoverflow.com/a/40359651
        bts_filtered = bts.loc[(bts.shift() != bts).any(axis=1)]

        # nb of agent in the system is the number of columns of bts len(bts.columns)
        # nb of binarized configuration met is the number of rows of the deduplicated bts : len(bts_dedup.index)
        # nb of transition observed is the number of rows in the ts in which the consecutive dupies have been filteres : len(bts_filtered.index) - 1
        rep.add_markdown(
            f"The system has {len(bts.columns)} agents, and thus can adopt 2^nb_agents = {2**len(bts.columns)} configurations in the boolean world. But {len(bts_dedup.index)} binarized config were observed. They were {len(bts_filtered.index) - 1} transition at total.")

        data_nbconfigtotal.append(2**len(bts.columns))
        data_nbconfigobserved.append(len(bts_dedup.index))

        # ----------
        # time unit from the sbml file
        m = sbmlparser.sbmlfile2libsbmlmodel(processedSBML)

        units = m.getTimeUnits()
        unitDefinitions = m.getUnitDefinition(units)
        rep.add_markdown(
            f"The time unit extracted from the sbml is : {units}, {unitDefinitions}")

        if not m.isSetTimeUnits():
            rep.add_markdown("**The time unit was not set in the sbml**")

    # ----------
    # Save the report

    filepath_report = args.outfolderpath / "main.html"
    filepath_report.parent.mkdir(parents=True, exist_ok=True)
    rep.write_report(filename=filepath_report)
    print(filepath_report)
    print("over")
