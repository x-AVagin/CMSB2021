

import argparse
import pathlib
import sys
import textwrap

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

try:
    sys.path.insert(1, 'code/')
except ImportError:
    print("is it the correct working directory ?")
    exit(1)
    # %pwd


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":

    # %% help message
    parser = argparse.ArgumentParser(
        description="""
        Produce a cumulative plot of the given measure.
        To be uses on benchmark files produced (for example) by snakemake
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--benchmarkfolder',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Folder where the benchmark files are stored.
                        ''')
                        )
    # parser.add_argument('--measure',
    #                     type=str,
    #                     required=True,
    #                     help=textwrap.dedent('''\
    #                     Name of the measure to plot. (it will be in abcisse and the # of files processed in ordinate)
    #                     ''')
    #                     )

    parser.add_argument('--sbmlList_file',
                        type=pathlib.Path,
                        required=False,
                        help=textwrap.dedent('''\
                        Optional. Path to the sbmlList file, which lists the SBML files to take into account.
                        The data points (from benchmarkfolder) are subsetset.
                        If no list is provided, all the points are plotted (from the benchmarkfolder).

                        Todo :
                        what is going on if no benchmark file is associated to an sbml from the list ?
                        does it exit ?
                        ''')
                        )

    parser.add_argument('--pathout',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path where to save the resulting image
                        ''')
                        )

    parser.add_argument('--timekill',
                        type=int,
                        required=False,
                        default=None,
                        help=textwrap.dedent('''\
                        If a value is given, plot a horizontal bar, and make the lines going up this bar.
                        Otherwise, the lines are stopping as soon as there are no more datapoints.
                        ''')
                        )

    parser.add_argument('--convert',
                        type=str,
                        const=True,
                        default="seconds",
                        help=textwrap.dedent('''\
                        In which unit to convert the time of the benchmark ?
                        Either "seconds" or "minutes" or "hours"
                        ''')
                        )

    parser.add_argument('--density',
                        type=str2bool,
                        nargs='?',
                        const=True,
                        default=True,
                        help=textwrap.dedent('''\
                        if True : ordinate axis is PROPORTION ; if False : it is NUMBER
                        ''')
                        )

    args = parser.parse_args()

    # %pwd
    # benchmarkfolder = pathlib.Path(
    #    "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/experiments/xp-runningexampleCMSB2021/benchmarks/")
    # benchmarkfolder = pathlib.Path(
    #    "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/results-20210316_CMSB3021_merge_1to10/benchmarks/")
    # benchmarkfolder = pathlib.Path(
    # "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/experiments/CMSB2021-results_merge-all_1to10/benchmarks/")
    # benchmarkfolder = pathlib.Path(
    #     "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/JOBIM2021/runtime/old/")
    benchmarkfolder = args.benchmarkfolder

    # pathout = "20210331_CMSB2021_cumulativeplot-cputime-hours.png"
    # pathout = "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/JOBIM2021/20210704_runtimecumul_oldonly.png"
    pathout = args.pathout

    # sbmlList_file = ""
    sbmlList_file = args.sbmlList_file

    # density = False
    density = args.density
    # if True : ordinate is PROPORTION ; if False : it is NUMBER

    # if False, uses https://stackoverflow.com/a/36630170, otherwist uses matplotlib.hist
    make_hist = True

    # convert = "hours"
    convert = args.convert

    # timekill = None
    timekill = args.timekill

    ######
    identificationmethods = [
        "reveal", "bestfit", "caspots", "our"]  # ["caspots", "our"]
    identificationmethods_colors = [
        "black", "green", "red", "blue"]
    identificationmethods_labels = [
        "REVEAL", "Best-Fit", "caspo-TS", "ASKeD-BN"]

    ###########################################################################

    f, (ax_boxplot, ax_histplot) = plt.subplots(
        2,
        sharex=True,
        figsize=(8, 4),
        gridspec_kw={"height_ratios": (.25, .75)})

    # ["caspots", "our"]:
    for index, identificationmethod in enumerate(identificationmethods):
        positionindex = index
        identificationmethod_color = identificationmethods_colors[positionindex]
        identificationmethod_label = identificationmethods_labels[positionindex]

        print(identificationmethod)

        # --------------------------------------------------------------------------
        # Determine the list of benchmark files to process

        # TODO : take sbmlList_file into account XXX
        # if not sbmlList_file:
        if True:
            # process all the benchmark files of a given identification method.
            benchmark_files = list(benchmarkfolder.glob(
                f"*.identification-{identificationmethod}.benchmark.txt"))
            benchmark_files
        else:
            # process only a subset.
            benchmark_files = []

            # extracted from the sbmlList_file (TODO) :
            sbmlList = ["a.sbml", "b.sbml"]
            sbmlIDList = ["a", "b"]

            for sbmlID in sbmlIDList:
                benchmark_file = pathlib.Path(
                    f"{sbmlID}.identification-{identificationmethod}.benchmark.txt")

                # if benchmark_file.is_file():
                # add even if it does not exist, the remaining of the code will take care of error
                benchmark_files.append(benchmark_file)

        # --------------------------------------------------------------------------
        COLUMNS = ["s", "h:m:s", "max_rss", "max_vms", "max_uss",
                   "max_pss", "io_in", "io_out", "mean_load", "cpu_time"]
        benchmark_data = []
        for filename in benchmark_files:
            print(filename)
            try:
                df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
                df["benchmark_file"] = filename

            except pd.errors.EmptyDataError:
                df = pd.DataFrame.from_dict({"benchmark_file": [filename]})
                for col in COLUMNS:
                    df[col] = np.inf

            # print(df)
            benchmark_data.append(df)

        benchmark_df = pd.concat(
            benchmark_data, axis=0, ignore_index=True, sort=True)
        # benchmark_df = pd.DataFrame(benchmark_data, columns=)
        benchmark_df.head()

        if convert == "seconds":
            x = benchmark_df.cpu_time
        elif convert == "minutes":
            x = benchmark_df.cpu_time / 60
            # conversion from seconds to mins
        elif convert == "hours":
            x = benchmark_df.cpu_time / 60 / 60
            # conversion from seconds to hours
        else:
            print("problem, unknown conversion")
            exit(1)

        print("xmin, xmax, len(benchmarks_files)")
        print(x.min(), x.max(), len(benchmark_files))

        if make_hist:
            ax_histplot.hist(
                x,
                range=(x.min(), x.max() if timekill == None else timekill),
                bins=len(benchmark_files),
                density=density,
                histtype='step',
                cumulative=True,
                label=identificationmethod_label,
                color=identificationmethod_color,
            )
            ax_histplot.axvline(x=30, color='white', linewidth=2)
            ax_histplot.axvline(x=30, color='black',
                                linewidth=2, linestyle='--')

        else:
            # https://stackoverflow.com/a/36630170 :
            binsCnt = len(benchmark_files)
            # bins = np.append(np.linspace(x.min(), x.max(), binsCnt), [np.inf])
            bins = list(range(0, int(x.max())))
            bins.append(np.inf)

            ax_histplot.hist(
                x,
                bins=bins,
                density=density,
                histtype='step',
                cumulative=True,
                label=identificationmethod_label,
                color=identificationmethod_color,
            )

        # ----------

        ax_boxplot.boxplot(
            x,
            vert=False,
            labels=[identificationmethod_label],
            positions=[positionindex],
            widths=0.75,  # larger of the box
            flierprops={
                "marker": 'o', "markerfacecolor": 'none', "markersize": 1,  # 5
                "markeredgecolor": 'black'
            },
            medianprops={
                # " linestyle"='-.', "linewidth"=2.5,
                "color": identificationmethod_color
            }
        )

    if timekill:
        ax_histplot.axvline(x=timekill, color='white', linewidth=2)
        ax_histplot.axvline(x=timekill, color='black',
                            linewidth=2, linestyle='--')
        ax_boxplot.axvline(x=timekill, color='white', linewidth=2)
        ax_boxplot.axvline(x=timekill, color='black',
                           linewidth=2, linestyle='--')

    ###########################################################################

    ax_histplot.grid(False)

    # plt.xscale("log")
    # ax.set_title('Cumulative step histograms')
    ax_histplot.set_xlabel(f'CPU time ({convert})')
    # plt.xscale("log")
    ax_histplot.set_ylabel('# systems processed')
    ax_histplot.legend(
        loc='center left',
        bbox_to_anchor=(1, 0.5))

    ###########################################################################

    # Save plot :
    plt.savefig(pathout, bbox_inches='tight')
