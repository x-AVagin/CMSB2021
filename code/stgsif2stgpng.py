import argparse
import sys
import textwrap

import networkx as nx
import PyBoolNet

try:
    sys.path.insert(1, 'code/')
    import stgplothandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


# PyBoolNet.Tests.Dependencies.run()


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":

    # %% help message
    parser = argparse.ArgumentParser(
        description="""
        Produce an image from a stg in sif format.
        Configurations found to have been observed in the binarized ts are highligted.
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--stgsif',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        sif file where stg is stored
                        ''')
                        )
    parser.add_argument('--colorobsconfigs',
                        type=str2bool,
                        nargs='?',
                        const=True,
                        default=False,
                        required=False,
                        help=textwrap.dedent('''\
                        whether to highlight the configurations observed in the binarized ts (the ones linked by a edge of the type 'obs')
                        ''')
                        )
    parser.add_argument('--drawobsseq',
                        type=str2bool,
                        nargs='?',
                        const=True,
                        default=False,
                        required=False,
                        help=textwrap.dedent('''\
                        whether to draw the sequence of the configuration observed in the binarized ts (the ones linked by an edge of the type 'obs')
                        ''')
                        )

    parser.add_argument('--outpng',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        path to the png file where to store the stg
                        ''')
                        )

    # %% Retrieve arguments
    args = parser.parse_args()
    # sys.argv = ["--stgsif", "test_stg.sif", "--colorobsconfigs", "--outpng", "test-stg.png"]
    # sys.argv = ["--stgsif", "test_stg.sif", "--colorobsconfigs", "--drawobsse", "--outpng", "test-stg.png"]
    # args = parser.parse_args(sys.argv)
    args.stgsif

    # -----
    # Recreate the stg as a networkx graph

    if args.drawobsseq:
        # if args.drawobsseq, we might (hopefully, actually) have parallel edges
        # hense use nx.MultiDiGraph insted of nx.DiGraph
        stg = nx.MultiDiGraph()
    else:
        stg = nx.DiGraph()

    obs = []
    with open(args.stgsif, 'r') as fsif:
        for line in fsif:
            source, kind, target = line.split()
            if kind == "stg":
                stg.add_edge(source, target)
            if kind == "obs" and (args.colorobsconfigs or args.drawobsseq):
                obs.append([source, target])

    if args.colorobsconfigs:
        for s, t in obs:
            stg.add_node(s)
            stg.add_node(t)
            stg.nodes[s]["color"] = "red"
            stg.nodes[t]["color"] = "red"

            if args.drawobsseq:
                stg.add_edge(s, t,
                             style="dashed",  # dashed, dotted, plain
                             color="red",
                             arrowsize=1
                             )

    #stg.graph["node"] = {"style": "filled", "color": "red"}
    #stg.graph["edge"] = {"arrowsize": 2.0}
    #stg.nodes["001000"]["fontsize"] = 20

    #PyBoolNet.StateTransitionGraphs.stg2image(stg, args.outpng)
    try:
        stgplothandler.stg2image(stg, args.outpng)
    except Exception:
        print("problem while creating the image. Exiting....")
        sys.exit(1)

    print("over")
    print(args.outpng)
    sys.exit(0)

    nx.draw(stg)
    pos = nx.spring_layout(stg)
    plt.savefig("STG.png", format="PNG")

    PyBoolNet.StateTransitionGraphs.add_style_sccs(
        stg)  # strongly connected components
    PyBoolNet.StateTransitionGraphs.stg2image(stg, "stg_from_stg_scc.dot")

    PyBoolNet.StateTransitionGraphs.create_image(
        primes, "asynchronous", "stg.dot")

    # STG from a given state :
    PyBoolNet.StateTransitionGraphs.create_image(
        primes,
        updatescheme,
        f"stg2_{updatescheme}.dot",
        InitialStates={"L": 0, "P": 1, "B": 0},
        # Styles=["anonymous", "tendencies", "mintrapspaces"]
        Styles=["anonymous", "mintrapspaces"]
    )

    state = PyBoolNet.StateTransitionGraphs.random_state(primes)
    local_igraph = PyBoolNet.InteractionGraphs.local_igraph_of_state(
        primes, state)
    PyBoolNet.InteractionGraphs.add_style_interactionsigns(local_igraph)
    PyBoolNet.InteractionGraphs.igraph2image(local_igraph, "local_igraph.dot")

    # Attractors :

    steady, cyclic = PyBoolNet.Attractors.compute_attractors_tarjan(stg)

    htg = PyBoolNet.StateTransitionGraphs.stg2htg(stg)
    PyBoolNet.StateTransitionGraphs.htg2image(htg, "htg.dot")
