import argparse
import collections
import os
import pathlib
import sys

import matplotlib.pyplot as plt
import mpld3
import pandas as pd

try:
    sys.path.insert(1, 'code/')
    import sbmlparser
    import tshandler
    from reports import Report

except ImportError:
    print("is it the correct working directory ?")
    exit(1)
    # %pwd


if __name__ == '__main__':

    rep = Report()

    parser = argparse.ArgumentParser(
        description='Description',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument('--xprespath',
                        help='Path to where the xp results are stored',
                        type=pathlib.Path,
                        required=True
                        )
    parser.add_argument('--pathout',
                        help='Path to where the report has to be saved.',
                        type=pathlib.Path,
                        required=True
                        )

    args = parser.parse_args()
    # sys.argv = ["--xprespath", "experiments/xpname"]
    # args = parser.parse_args(sys.argv)

    args.pathout.mkdir(parents=True, exist_ok=True)

    # ---
    # Data used for plots later :
    data_nbconfigtotal = []
    data_nbconfigobserved = []
    # ---

    processedSBMLs = []
    sbmllist_filepath = args.xprespath / "sbml-list.txt"
    with open(sbmllist_filepath, 'r') as f:
        for line in f:
            processedSBMLs.append(line.strip())

    for processedSBML in processedSBMLs:

        BIOMD_id = os.path.splitext(os.path.basename(processedSBML))[0]

        # open the binarized ts :
        bts = tshandler.tsfile2pddf(
            args.xprespath / "ts" / f"{BIOMD_id}_binarizedtsts.csv")

        # remove duplicated lines to get the number of configation that were met
        bts_dedup = bts.drop_duplicates(inplace=False)

        # remove consecutive duplicated to get the transition that were observed
        # Code to remove consecutive duplicates from : https://stackoverflow.com/a/40359651
        bts_filtered = bts.loc[(bts.shift() != bts).any(axis=1)]

        # nb of agent in the system is the number of columns of bts len(bts.columns)
        # nb of binarized configuration met is the number of rows of the deduplicated bts : len(bts_dedup.index)
        # nb of transition observed is the number of rows in the ts in which the consecutive dupies have been filteres : len(bts_filtered.index) - 1

        data_nbconfigtotal.append(2**len(bts.columns))
        data_nbconfigobserved.append(len(bts_dedup.index))

    # ----------
    # Data that we have :

    # - biomdid
    biomdid = [os.path.splitext(os.path.basename(processedSBML))[0]
               for processedSBML in processedSBMLs]
    # - data_proportion
    data_proportion = [obs / total for obs,
                       total in zip(data_nbconfigobserved, data_nbconfigtotal)]
    # - data_nbconfigtotal

    # ----------
    # Save the data
    data_to_save = pd.DataFrame({
        "biomdid": biomdid,
        "proportion_visited": data_proportion,
        "nbconfigtotal": data_nbconfigtotal
    })

    data_to_save.sort_values(
        inplace=True,
        by=['proportion_visited', 'nbconfigtotal'],
        ascending=False
    )
    data_to_save.to_csv(
        args.pathout / "table_nbconfigVSvisited.csv", index=False)

    # ----------
    # Histogram proportion of configuration visited (compared to the total nb of possible configuration)
    rep.add_markdown(
        "Histogram proportion of configuration visited (compared to the total nb of possible configuration)")

    plt.hist(data_proportion)
    rep.add_figure()

    # ----------
    # Plot 2D with abs : nb confi total ; ordo : nbconfigobserved
    rep.add_markdown(
        "Plot 2D with abs : nb confi total ; ordo : proportion de config visité")

    labels = [f"""
              {os.path.splitext(os.path.basename(processedSBML))[0]}
              x = {data_nbconfigtotal[i]}
              y = {data_proportion[i]}
              """
              for i, processedSBML in enumerate(processedSBMLs)]

    # Version non interactive:
    rep.add_markdown("non interactive version")
    fig, ax = plt.subplots()
    ax.set_xscale('log', base=10)

    scatter = ax.scatter(data_nbconfigtotal, data_proportion)

    for i, label in enumerate(labels):
        ax.annotate(
            label,
            (data_nbconfigtotal[i], data_proportion[i]),
        )
    rep.add_figure()

    # ---
    # Version interactive:
    # Bug : Incorrect mpld3 scatter plotting in log scale
    # https://github.com/mpld3/mpld3/issues/227

    # fig, ax = plt.subplots()
    # ax.set_xscale('log', base=10)
    # scatter = ax.scatter(data_nbconfigtotal, data_proportion)
    # tooltips = mpld3.plugins.PointLabelTooltip(scatter, labels=labels)
    # mpld3.plugins.connect(fig, tooltips)
    # rep.add_ifig(fig)

    # ---
    # proposed workarround : use plot instead of scatter
    # Note that the coordinated are accessed with `points[0]` and not just `points`
    # https://github.com/mpld3/mpld3/issues/227#issuecomment-735251460
    rep.add_markdown(
        "interactive version : workaround = plot instead of scatter")

    fig, ax = plt.subplots()
    ax.grid(True, alpha=0.3)

    points = ax.plot(data_nbconfigtotal, data_proportion, 'o', color='b',
                     mec='k', ms=15, mew=1, alpha=.6)

    ax.set_xlabel('x')
    ax.set_xscale('log', base=10)
    ax.set_ylabel('y')
    ax.set_title('HTML tooltips', size=20)
    tooltips = mpld3.plugins.PointLabelTooltip(points[0], labels=labels)
    # Or use html tooltips :
    # https://mpld3.github.io/examples/html_tooltips.html
    # tooltips = mpld3.plugins.PointHTMLTooltip(
    #     points[0], labels,
    #     voffset=10, hoffset=10
    # )
    mpld3.plugins.connect(fig, tooltips)
    rep.add_ifig(fig)
    plt.close('all')
    # ---
    # Try to invert the axis (did not work either)

    # ---
    # Try to plot -y instead of y (did not work either)

    # data_proportion_inverted = [-p for p in data_proportion]
    # fig, ax = plt.subplots()
    # ax.set_xscale('log', base=10)
    # scatter = ax.scatter(data_nbconfigtotal, data_proportion_inverted)
    # tooltip = mpld3.plugins.PointLabelTooltip(scatter, labels=labels)
    # mpld3.plugins.connect(fig, tooltip)
    # rep.add_ifig(fig)

    # ----------
    # Subset only the models having a proportion visited > 50%
    data_subset = data_to_save[data_to_save.proportion_visited >= 0.5]
    print(data_subset)

    rep.add_markdown("______________")
    rep.add_markdown(
        "Taking only the models having a proportion visited > 50%")
    rep.add_markdown(str(data_subset.shape))

    # ----------
    # Histogram proportion of configuration visited (compared to the total nb of possible configuration)
    rep.add_markdown(
        "Histogram proportion of configuration visited (compared to the total nb of possible configuration)")
    plt.hist(data_subset.proportion_visited)
    rep.add_figure()

    # ----------
    # Plot 2D with abs : nb confi total ; ordo : nbconfigobserved
    rep.add_markdown(
        "Plot 2D with abs : nb confi total ; ordo : proportion de config visité")
    rep.add_markdown(
        "interactive version : scatter without log axis")

    fig, ax = plt.subplots()
    # ax.set_xscale('log', base=10) # do not log the axis : scatter with log axis cannot be used with mpld3
    # count the occurrences of each point
    c = collections.Counter(
        zip(data_subset["nbconfigtotal"], data_subset["proportion_visited"]))
    # create a list of the sizes, here multiplied by 10 for scale
    s = [10 * c[(xx, yy)]
         for xx, yy in zip(data_subset["nbconfigtotal"], data_subset["proportion_visited"])]
    scatter = ax.scatter(
        data_subset["nbconfigtotal"], data_subset["proportion_visited"], s=s)

    # list of list storing labels = biomodelsid for each coordinates in the scatter plot
    labels = []
    for xx, yy in zip(data_subset["nbconfigtotal"], data_subset["proportion_visited"]):
        print(data_subset.loc[
            (data_subset['nbconfigtotal'] == xx)
            & (data_subset['proportion_visited'] == yy)
        ])
        labels.append(data_subset.loc[
            (data_subset['nbconfigtotal'] == xx)
            & (data_subset['proportion_visited'] == yy)
        ].biomdid.to_list()
        )
    print(labels)
    labels = ['\n'.join(elements) for elements in labels]
    tooltips = mpld3.plugins.PointLabelTooltip(
        scatter, labels=labels)
    mpld3.plugins.connect(fig, tooltips)
    rep.add_ifig(fig)

    # ----------
    # Save the report

    filepath_report = args.pathout / "htmlreport_compare-ts-with-curatedts.html"

    rep.write_report(filename=filepath_report)
    print(filepath_report)
    print("over")
