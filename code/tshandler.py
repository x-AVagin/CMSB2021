
import logging

import pandas as pd
from sklearn import preprocessing

logger = logging.getLogger('logger.tshandler')


def ts2stretchedts(timecourse, globally, minstretched=-100, maxstretched=100):

    # define the scaler :
    scaler = preprocessing.MinMaxScaler(
        feature_range=(minstretched, maxstretched), copy=False)

    if globally:
        # fit the scaler globally
        # find min and max accross all the columns on the dataframe
        globalmin = min(timecourse.min())
        globalmax = max(timecourse.max())
        scalertrainer = [
            [e for i in range(len(timecourse.columns))]
            for e in [globalmin, globalmax]
        ]

        fittedscaler = scaler.fit(scalertrainer)
        # then use the fitted scaler :
        timecourse = pd.DataFrame(
            fittedscaler.transform(timecourse),
            columns=timecourse.columns,
            index=timecourse.index
        )
    else:
        # use the default behavior of the sclare which works component-wise :
        timecourse = pd.DataFrame(
            scaler.fit_transform(timecourse),
            columns=timecourse.columns,
            index=timecourse.index
        )

    # Note : if a feature is stricly constant, the MinMax scaler uses the minimum of the feature range (-> -100)
    # we would like it to use 0 instead:
    constant_cols = timecourse.columns[timecourse.nunique() <= 1]
    logger.info(f"These columns have constant observatons: {constant_cols}")
    for col in constant_cols:
        timecourse[col] = 0

    timecourse = timecourse.astype(int)

    return timecourse


def binarization(timecourse, treasold=0):
    """
    Binarized ts to -1 and 1 values
    """

    timecourse_binarized = timecourse.ge(treasold).astype(int)
    # ge = Get Greater than

    # ---
    # 0 replaced by -1 :
    # we do it after the inconsistency check
    # because if no inconsistency has been spotted,
    # the consecutive dplicates have been removed from the time serie
    # so their is less work to do for the replacement of value

    # Used to throw SettingWithCopyWarning warning because it was a modification of a copy
    timecourse_binarized.replace(
        inplace=True, to_replace=0, value=-1)
    return timecourse_binarized


def tsfile2stretchedts(tsfilepath, minstretched=-100, maxstretched=100):
    ts = tsfile2pddf(tsfilepath)
    ts = ts2stretchedts(ts, minstretched, maxstretched)
    return ts


def tsfile2pddf(tsfilepath):
    """
    Get a 'working' pd df representing the ts

    Arguments :
    -----------
    - tsfilepath (str)
        filepath of csv storing the ts

    Returns :
    ---------
    a pandas dataframe.
    One column per csv column
    First column (time) as rowname
    Colnames are lowercased

    """

    timecourse = pd.read_csv(
        tsfilepath,
        sep=",",
        index_col=[0],  # use time as rowname
        skipinitialspace=True  # Skip spaces after delimiter
    )

    # assert no empty col at the end of the ts.csv
    # (the rule runsimu used to "randomly" add empty col at the end of the ts.csv
    # problem has been fixed, but still keep this test to be notified if pb reappears...)
    if timecourse.iloc[:, -1].isnull().all():
        # timecourse.drop(timecourse.columns[len(
        #    timecourse.columns) - 1], axis=1, inplace=True)
        logger.error("last column of timecourse is empty !! ")
        exit(1)

    # timecourse.rename(columns=lambda x: x.strip().lower(), inplace=True)
    # timecourse.columns = map(str.lower, timecourse.columns)

    # TODO :
    # assert column in the file was sorted in alphabetical order.
    # send a warning otherwise

    return timecourse
